package client.testRoot2;

import java.util.Date;

import client.delegate.examManagement.ExamManagementDelegate;

import tn.edu.pdev.moocs.domain.Certification;

public class CreateCertification {
	
	public static void main(String[] args) {
		
		Certification certification = new Certification(new Date(), 120);
		
		ExamManagementDelegate.createCertification(certification);
	}

}
