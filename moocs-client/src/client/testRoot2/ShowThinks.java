package client.testRoot2;

import java.util.List;

import client.delegate.courseManagement.CouseManagementDelegate;
import client.delegate.examManagement.ExamManagementDelegate;
import tn.edu.pdev.moocs.domain.Answers;
import tn.edu.pdev.moocs.domain.Chapter;
import tn.edu.pdev.moocs.domain.Question;

public class ShowThinks {

	public static void main(String[] args) {
		
		Chapter chapter = CouseManagementDelegate.findChapter(1);
		List<Answers> answers;
		List<Question> questions = ExamManagementDelegate.findAllQuestionByChap(chapter);
		
		for (Question question : questions) {
			System.out.println(question.getNameQuestion());
			System.out.println("****\n");
			answers = ExamManagementDelegate.finAllAnserwerByQuestion(question);
			for (Answers answers2 : answers) {
				System.out.println(answers2.getAnnser1());
				System.out.println(answers2.getAnnser2());
				System.out.println(answers2.getAnnser3());
		
			}
		}
		
	}
}
