package client.testRoot2;

import client.delegate.examManagement.ExamManagementDelegate;
import client.delegate.profilsManagement.ProfilsManagementDelegate;
import tn.edu.pdev.moocs.domain.Quiz;
import tn.edu.pdev.moocs.domain.Teacher;

public class AffectQuizToTeacher {

	public static void main(String[] args) {
		Teacher teacher = ProfilsManagementDelegate.findTeacher(3);
		Quiz quiz = ExamManagementDelegate.findQuiz(52);
		
		quiz.setTeacher(teacher);
		
		ExamManagementDelegate.updateQuiz(quiz);
		
	}
}
