package client.testRoot2;

import client.delegate.examManagement.ExamManagementDelegate;
import tn.edu.pdev.moocs.domain.Question;

public class CreateSomeQuestion {

	public static void main(String[] args) {
		

		Question question1 = new Question(1, "Which one of the following is not one of standard stereotype in Deployment Diagram?");
		Question question2 = new Question(2, "Which of the following relationship is not part of package diagram ?");
		Question question3 = new Question(3, "What are the two broad categories of diagrams in UML 2.0?");
		Question question4 = new Question(4, "Which of the following is not a valid type of message arrow in sequence diagram?");
		Question question5 = new Question(5, "A class diagram can be used to model following things except?");
		Question question6 = new Question(6, "Which of the following is not valid in the context of Component Diagrams?");


		ExamManagementDelegate.createQuestion(question1);
		ExamManagementDelegate.createQuestion(question2);
		ExamManagementDelegate.createQuestion(question3);
		ExamManagementDelegate.createQuestion(question4);
		ExamManagementDelegate.createQuestion(question5);
		ExamManagementDelegate.createQuestion(question6);		
	}
	
	
}
