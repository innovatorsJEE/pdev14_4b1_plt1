package client.testRoot2;

import java.util.Date;

import client.delegate.examManagement.ExamManagementDelegate;

import tn.edu.pdev.moocs.domain.Quiz;

public class CreateSomeQuiz {

	public static void main(String[] args) {
		
		Quiz quiz = new Quiz(16, new Date(), "UML");
		
		ExamManagementDelegate.createQuiz(quiz);
	
	}
	
}
