package client.testRoot2;

import client.delegate.examManagement.ExamManagementDelegate;
import tn.edu.pdev.moocs.domain.Answers;

public class CreateSomeAns {

	
	public static void main(String[] args) {
		
		Answers answers1  = new Answers("artifact", "processor", " device",2);
		Answers answers2  = new Answers("Merge", "Import", "Export",3);
		Answers answers3  = new Answers("Structural and Use Case", "Structural and Behavioral", "Behavioral and Logical",2);
		Answers answers4  = new Answers("Synchronous Messages", "Activation Message", "Asynchronous Messages",2);
		Answers answers5  = new Answers("Depicting the interaction between domain entities", "Depicting the implementation detail of domain entities", "Depicting state and behavior of the domain entity",1);
		Answers answers6  = new Answers("Black-box and While-box view", "ports and connectors", "initial node, final node",3);

		ExamManagementDelegate.createAnswers(answers1);
		ExamManagementDelegate.createAnswers(answers2);
		ExamManagementDelegate.createAnswers(answers3);
		ExamManagementDelegate.createAnswers(answers4);
		ExamManagementDelegate.createAnswers(answers5);
		ExamManagementDelegate.createAnswers(answers6);
		
	}
}
