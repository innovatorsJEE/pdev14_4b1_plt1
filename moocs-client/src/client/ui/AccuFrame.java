package client.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.SystemColor;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Font;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.JButton;

import tn.edu.pdev.moocs.domain.Admin;
import tn.edu.pdev.moocs.domain.Student;
import tn.edu.pdev.moocs.domain.Teacher;
import tn.edu.pdev.moocs.domain.User;

import client.delegate.profilsManagement.ProfilsManagementDelegate;
import client.sessions.Session;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class AccuFrame extends JFrame {

	private JPanel contentPane;
	private JTextField tf_log;
	private JPasswordField tf_pass;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AccuFrame frame = new AccuFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AccuFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 722, 412);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.window);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.window);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 696, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 369, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(57, Short.MAX_VALUE))
		);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBackground(Color.WHITE);
		lblNewLabel.setIcon(new ImageIcon(AccuFrame.class.getResource("/client/ui/mooc-course.jpg")));
		
		JLabel label = new JLabel("");
		
		JLabel label_1 = new JLabel("Login");
		label_1.setForeground(Color.RED);
		label_1.setFont(new Font("Lucida Handwriting", Font.BOLD, 12));
		
		tf_log = new JTextField();
		tf_log.setBackground(UIManager.getColor("Button.background"));
		tf_log.setColumns(10);
		
		JLabel label_2 = new JLabel("Password\r\n");
		label_2.setForeground(Color.RED);
		label_2.setFont(new Font("Lucida Handwriting", Font.BOLD, 12));
		
		JLabel lblNewLabel_1 = new JLabel("PLEASE LOG IN");
		lblNewLabel_1.setForeground(SystemColor.activeCaption);
		lblNewLabel_1.setFont(new Font("Trebuchet MS", Font.PLAIN, 15));
		
		JLabel lblToAccessYour = new JLabel("to access your account and courses");
		lblToAccessYour.setForeground(UIManager.getColor("Button.darkShadow"));
		lblToAccessYour.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		
		JButton btnNewButton = new JButton("Log into My Account ");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			try
			{
				String log = tf_log.getText();
				String pass = tf_pass.getText();
				
				User user = ProfilsManagementDelegate.autehnticate(log, pass);
				
//				if(user instanceof Student)
//				{
//					user.setAct(true);
//				}
				if(user instanceof Admin)
				{
					user.setAct(true);
				}
				if(user instanceof Student)
				{
					user.setAct(true);
				}
				
				
			if(user.isAct() == true)	
			{
				if(user != null)
				{
					System.out.println(user.getId());
					Session.getInstance().setUser(user);
					
					if(user instanceof Student)
					{

						System.out.println("Student !!");
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									StudentAccountFrame frame = new StudentAccountFrame();
									frame.setVisible(true);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
						setVisible(false);

//						
					}
					
					else if (user instanceof Teacher) {
						System.out.println("Teacher !!");
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									TeacherAccountFrame frame = new TeacherAccountFrame();
									frame.setVisible(true);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
						setVisible(false);

					}
					else if(user instanceof Admin){
						System.out.println("admin !!");
				        java.awt.EventQueue.invokeLater(new Runnable() {
				            public void run() {
				                new AdminAccountFrame().setVisible(true);
				            }
				        });
				        setVisible(false);
					}
					
				}
			}
			else
			{
				JOptionPane.showMessageDialog(null,"Account is disabled!!","WORRING",JOptionPane.ERROR_MESSAGE);
			}
			
			}
	         catch (NullPointerException npe) {
		            JOptionPane.showMessageDialog(null, "Input ERROR !!", "Probl�me de format", JOptionPane.ERROR_MESSAGE);
		          }
			
			//
			}
		});
		btnNewButton.setBackground(new Color(192, 192, 192));
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		
		tf_pass = new JPasswordField();
		tf_pass.setBackground(UIManager.getColor("Button.background"));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(lblNewLabel)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(18)
							.addComponent(label))
						.addGroup(gl_panel.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel_1)
								.addGroup(gl_panel.createSequentialGroup()
									.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
										.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
										.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
										.addComponent(tf_pass)
										.addComponent(tf_log, GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)
										.addComponent(btnNewButton, Alignment.TRAILING)))))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(28)
							.addComponent(lblToAccessYour)))
					.addContainerGap(34, Short.MAX_VALUE))
				.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 696, Short.MAX_VALUE)
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(40)
							.addComponent(label)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblNewLabel_1)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lblToAccessYour)
							.addGap(18)
							.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
								.addComponent(tf_log, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
								.addComponent(tf_pass, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(27)
							.addComponent(btnNewButton)))
					.addGap(34)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		JLabel label_4 = new JLabel("Registering with MOOCs gives you access to all of our current and future free courses.");
		label_4.setForeground(new Color(255, 140, 0));
		label_4.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		
		JLabel lblNewLabel_2 = new JLabel("ready to take a courses ? ");
		lblNewLabel_2.setForeground(Color.RED);
		lblNewLabel_2.setBackground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		
		JButton btnJoinUs = new JButton("Join us");
		btnJoinUs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				CreateUserFrame createUserFrame = new CreateUserFrame();
				createUserFrame.setVisible(true);
				setVisible(false);
			}
		});
		btnJoinUs.setFont(new Font("Arial Unicode MS", Font.PLAIN, 14));
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(72)
							.addComponent(label_4, GroupLayout.PREFERRED_SIZE, 545, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(255)
							.addComponent(lblNewLabel_2))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(296)
							.addComponent(btnJoinUs)))
					.addContainerGap(79, Short.MAX_VALUE))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(label_4, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblNewLabel_2)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnJoinUs)
					.addContainerGap(74, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
	}

}
