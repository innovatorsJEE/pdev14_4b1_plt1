package client.ui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;

import tn.edu.pdev.moocs.domain.Question;
import tn.edu.pdev.moocs.domain.Quiz;
import client.delegate.examManagement.ExamManagementDelegate;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class QuestionManagementFrame extends JFrame {

	private JPanel contentPane;
	private JTable tQuestion;
	private JTextField tfQuestion;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					QuestionManagementFrame frame = new QuestionManagementFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	List<Question> questions=new ArrayList<>();
	Question questionSelected=new Question();
	List<Quiz> quizs= new ArrayList<>();
	Quiz quizSelected = new Quiz();
	private String test;
	final JComboBox cbQuiz;
	JButton btnUpdateQuestion;
	JButton btnDelete;
	/**
	 * Create the frame.
	 */
	public QuestionManagementFrame() {
//		btnDelete.setEnabled(true);
		cbQuiz = new JComboBox ();
		questions = ExamManagementDelegate.findAllQuestion();
		quizs = ExamManagementDelegate.findAllQuiz();
		cbQuiz.addItem("Select a Quiz");
		for(Quiz quiz :quizs)
			cbQuiz.addItem(quiz.getNameQuiz());
		cbQuiz.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				test = cbQuiz.getSelectedItem().toString();
				System.out.println(test);
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 724, 506);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Questions", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(0, 0, 255)));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Question Management", TitledBorder.LEFT, TitledBorder.TOP, null, Color.BLUE));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 698, Short.MAX_VALUE)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 698, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 272, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(15, Short.MAX_VALUE))
		);
		
		tfQuestion = new JTextField();
		tfQuestion.setText("");
		tfQuestion.setColumns(10);
		
		JLabel lblQuestion = new JLabel("Question");
		
		btnUpdateQuestion = new JButton("update");
		btnUpdateQuestion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				questionSelected.setNameQuestion(tfQuestion.getText());
				test = cbQuiz.getSelectedItem().toString();
				questionSelected.setQuestionQuiz(test);
				ExamManagementDelegate.updateQuestion(questionSelected);
				questions = ExamManagementDelegate.findAllQuestion();
				initDataBindings();
				clear();
				
			}
		});
		
		JButton btnClear = new JButton("clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				questionSelected= new Question();
				clear();
			}
		});
		
		JButton btnManageAnswer = new JButton("Manage Answer");
		btnManageAnswer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AnswerManagementFrame frame = new AnswerManagementFrame();
				frame.setVisible(true);
				setVisible(false);
			}
		});
		
		JButton btnBackToProfil = new JButton("Back to Profil");
		btnBackToProfil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TeacherAccountFrame frame = new TeacherAccountFrame();
				frame.setVisible(true);
				setVisible(false);
			}
		});
		
		btnDelete = new JButton("delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				questionSelected = questions.get(tQuestion.getSelectedRow());
				ExamManagementDelegate.deleteQuestion(questionSelected);
				questions = ExamManagementDelegate.findAllQuestion();
				initDataBindings();
				clear();
			}
		});
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(30)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnClear)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addComponent(lblQuestion)
							.addGap(18)
							.addComponent(tfQuestion, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)))
					.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(133)
							.addComponent(btnBackToProfil)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnManageAnswer)
							.addContainerGap())
						.addGroup(Alignment.LEADING, gl_panel_1.createSequentialGroup()
							.addGap(56)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(btnDelete)
								.addComponent(btnUpdateQuestion))
							.addContainerGap(196, Short.MAX_VALUE))))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(43)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(tfQuestion, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblQuestion)
						.addComponent(btnUpdateQuestion))
					.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnClear)
							.addContainerGap(42, Short.MAX_VALUE))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(3)
							.addComponent(btnDelete)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnManageAnswer)
								.addComponent(btnBackToProfil))
							.addContainerGap())))
		);
		panel_1.setLayout(gl_panel_1);
		
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 666, Short.MAX_VALUE)
						.addComponent(cbQuiz, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(cbQuiz, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(12)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 186, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(20, Short.MAX_VALUE))
		);
		
		tQuestion = new JTable();
		tQuestion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				questionSelected = questions.get(tQuestion.getSelectedRow());
				tfQuestion.setText(questionSelected.getNameQuestion());
				
			}
		});
		scrollPane.setViewportView(tQuestion);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
		initDataBindings();
	}
	protected void initDataBindings() {
		JTableBinding<Question, List<Question>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE, questions, tQuestion);
		//
		BeanProperty<Question, String> questionBeanProperty = BeanProperty.create("questionQuiz");
		jTableBinding.addColumnBinding(questionBeanProperty).setColumnName("Quiz");
		//
		BeanProperty<Question, String> questionBeanProperty_1 = BeanProperty.create("nameQuestion");
		jTableBinding.addColumnBinding(questionBeanProperty_1).setColumnName("Question");
		//
		jTableBinding.bind();
	}
	private void clear() {
		tfQuestion.setText("");
	}
}
