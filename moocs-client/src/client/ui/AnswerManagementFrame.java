package client.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;

import tn.edu.pdev.moocs.domain.Answers;
import tn.edu.pdev.moocs.domain.Question;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.ObjectProperty;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.Bindings;

import client.delegate.examManagement.ExamManagementDelegate;
import client.sessions.SessionTeacher;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AnswerManagementFrame extends JFrame {

	private JPanel contentPane;
	private JTextField tfAnswer;
	private JTable tQuestion;
	private JTable tAnswer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AnswerManagementFrame frame = new AnswerManagementFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	List<Question>questions = new ArrayList<>();
	Question questionSelected = new Question();
	List<Answers> answerss = new ArrayList<>();
	Answers answersSelected = new Answers();
	JComboBox cbEtat;
	

	/**
	 * Create the frame.
	 */
	public AnswerManagementFrame() {
		questions = ExamManagementDelegate.findAllQuestion();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 682, 558);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Questions", TitledBorder.LEFT, TitledBorder.TOP, null, Color.BLUE));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Answers", TitledBorder.LEFT, TitledBorder.TOP, null, Color.BLUE));
		
		JPanel panel_2 = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 655, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 654, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 657, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		JLabel lblAnswer = new JLabel("Answer");
		
		JLabel lblEtat = new JLabel("Etat");
		
		cbEtat = new JComboBox();
		cbEtat.addItem("true");
		cbEtat.addItem("fasle");
		
		tfAnswer = new JTextField();
		tfAnswer.setColumns(10);
		
		JButton btnUpdate = new JButton("update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Answers answers=new Answers();
				answers.setReponse(tfAnswer.getText());
				answers.setEtat(cbEtat.getSelectedItem().toString());
				ExamManagementDelegate.updateAnswers(answersSelected);
				answerss.add(answers);
				initDataBindings();
				clear();
			}
		});
		
		JButton btnClear = new JButton("clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Answers answers= new Answers();
				clear();
				
			}
		});
		
		JButton btndelete = new JButton("delete");
		btndelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				answersSelected = answerss.get(tAnswer.getSelectedRow());
				ExamManagementDelegate.deleteAnswers(answersSelected);
				answerss = ExamManagementDelegate.getAnswersByQuestion(SessionTeacher.getInstance().getQuestion());
				initDataBindings();
				clear();
				
			}
		});
		JButton btnBackToProfil = new JButton("Back To Profil");
		btnBackToProfil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TeacherAccountFrame frame = new  TeacherAccountFrame();
				frame.setVisible(true);
				setVisible(false);
			}
		});
		
		JButton btnValid = new JButton("valid");
		btnValid.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(Answers answers:answerss){
				answers.setQuestion(questionSelected);
				ExamManagementDelegate.updateAnswers(answers);
				}
				initDataBindings();
			}
		});
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING, false)
						.addComponent(lblEtat, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblAnswer, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(18)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(btnClear)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(cbEtat, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE))
						.addComponent(tfAnswer, 144, 144, 144))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(320)
							.addComponent(btnBackToProfil))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(btnUpdate)
							.addGap(40)
							.addComponent(btnValid))
						.addComponent(btndelete))
					.addGap(22))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap(26, Short.MAX_VALUE)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblAnswer)
								.addComponent(tfAnswer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnUpdate)
								.addComponent(btnValid))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
								.addComponent(btndelete)
								.addComponent(cbEtat, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblEtat)
								.addComponent(btnClear)))
						.addComponent(btnBackToProfil))
					.addGap(35))
		);
		panel_2.setLayout(gl_panel_2);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 628, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		tAnswer = new JTable();
		tAnswer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				answersSelected =  answerss.get(tAnswer.getSelectedRow());
				tfAnswer.setText(answersSelected.getReponse());
			}
		});
		scrollPane_1.setViewportView(tAnswer);
		panel_1.setLayout(gl_panel_1);
		
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnGetQuestion = new JButton("get answers");
		btnGetQuestion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				answerss = ExamManagementDelegate.getAnswersByQuestion(questionSelected);
				initDataBindings();
			}
		});
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 624, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap(543, Short.MAX_VALUE)
					.addComponent(btnGetQuestion)
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
					.addComponent(btnGetQuestion)
					.addContainerGap())
		);
		
		tQuestion = new JTable();
		tQuestion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				questionSelected = questions.get(tQuestion.getSelectedRow());
				SessionTeacher.getInstance().setQuestion(questionSelected);
				
			}
		});
		scrollPane.setViewportView(tQuestion);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
		initDataBindings();
	}
	private void clear() {
		questionSelected = new Question();
		tfAnswer.setText("");
	}
	protected void initDataBindings() {
		JTableBinding<Answers, List<Answers>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ, answerss, tAnswer);
		//
		BeanProperty<Answers, String> answersBeanProperty = BeanProperty.create("question.nameQuestion");
		jTableBinding.addColumnBinding(answersBeanProperty).setColumnName("Question");
		//
		BeanProperty<Answers, String> answersBeanProperty_1 = BeanProperty.create("reponse");
		jTableBinding.addColumnBinding(answersBeanProperty_1).setColumnName("Answer");
		//
		BeanProperty<Answers, String> answersBeanProperty_2 = BeanProperty.create("etat");
		jTableBinding.addColumnBinding(answersBeanProperty_2).setColumnName("true/false");
		//
		jTableBinding.bind();
		//
		JTableBinding<Question, List<Question>, JTable> jTableBinding_1 = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE, questions, tQuestion);
		//
		BeanProperty<Question, Integer> questionBeanProperty = BeanProperty.create("id");
		jTableBinding_1.addColumnBinding(questionBeanProperty).setColumnName("number Question");
		//
		BeanProperty<Question, String> questionBeanProperty_1 = BeanProperty.create("nameQuestion");
		jTableBinding_1.addColumnBinding(questionBeanProperty_1).setColumnName("Question");
		//
		BeanProperty<Question, String> questionBeanProperty_2 = BeanProperty.create("questionQuiz");
		jTableBinding_1.addColumnBinding(questionBeanProperty_2).setColumnName("Quiz");
		//
		jTableBinding_1.bind();
	}
}
