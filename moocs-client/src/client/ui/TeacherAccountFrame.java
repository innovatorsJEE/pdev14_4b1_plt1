package client.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Color;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.JLayeredPane;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import tn.edu.pdev.moocs.domain.Quiz;

import client.delegate.examManagement.ExamManagementDelegate;
import client.sessions.Session;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import javax.swing.border.TitledBorder;
import org.jdesktop.beansbinding.ObjectProperty;
import org.jdesktop.beansbinding.BeanProperty;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

public class TeacherAccountFrame extends JFrame {

	private JPanel contentPane;
	private JTable tQuiz;

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TeacherAccountFrame frame = new TeacherAccountFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	List<Quiz>quizs = new ArrayList<>();
	Quiz quizSelected = new Quiz();
	private JTextField tfNameQuiz;
	final JButton btnDeleteQuiz;
	final JButton btnUpdateQuiz;
	/**
	 * Create the frame.
	 */
	public TeacherAccountFrame() {
		quizs = ExamManagementDelegate.findAllQuiz();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 704, 474);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JTabbedPane TeacherProfil = new JTabbedPane(JTabbedPane.TOP);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(TeacherProfil, GroupLayout.PREFERRED_SIZE, 677, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(TeacherProfil, GroupLayout.DEFAULT_SIZE, 415, Short.MAX_VALUE))
		);
		
		JLayeredPane profil = new JLayeredPane();
		TeacherProfil.addTab("Profil", null, profil, null);
		
		JPanel corrdonnePanel1 = new JPanel();
		
		JLabel label = new JLabel("CIN :");
		label.setForeground(Color.BLUE);
		
		JLabel label_1 = new JLabel("date of birth :");
		label_1.setForeground(Color.BLUE);
		
		JLabel label_2 = new JLabel(" Sexe :");
		label_2.setForeground(Color.BLUE);
		
		JLabel label_3 = new JLabel("E-mail :");
		label_3.setForeground(Color.BLUE);
		
		JLabel label_4 = new JLabel("Last Name : ");
		label_4.setForeground(Color.BLUE);
		
		JLabel label_5 = new JLabel("First Name : ");
		label_5.setForeground(Color.BLUE);
		
		JButton button_1 = new JButton("Edit Profil");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TeacherUpdate update = new TeacherUpdate();
				update.setVisible(true);
				setVisible(false);
				
			}
		});
		
		JLabel cinDynamic1 = new JLabel(" "+Session.getInstance().getUser().getCin());
		
		JLabel lastNameDynamic1 = new JLabel(" "+Session.getInstance().getUser().getLastName());
		
		JLabel dateOfBirthDynamic1 = new JLabel(" "+Session.getInstance().getUser().getDateOfBirth() );
		
		JLabel firstNameDynamic1 = new JLabel(" "+Session.getInstance().getUser().getFirstName());
		
		JLabel sexeDynamic1 = new JLabel(" "+Session.getInstance().getUser().getSexe());
		
		JLabel emailDynamic1 = new JLabel(" "+Session.getInstance().getUser().getMail());
		
		JButton btnNewButton_3 = new JButton("Disconnect");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AccuFrame accuFrame = new AccuFrame();
				
				accuFrame.setVisible(true);
				setVisible(false);
				
			}
		});
		
		JButton btnCoursesManagment = new JButton("Courses Managment");
		btnCoursesManagment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TeacherFrame teacherFrame = new TeacherFrame();
				teacherFrame.setVisible(true);
				setVisible(false);
			}
		});
		
		JLabel label_6 = new JLabel("");
		label_6.setIcon(new ImageIcon(TeacherAccountFrame.class.getResource("/client/ui/teacher_icon.jpg")));
		GroupLayout gl_profil = new GroupLayout(profil);
		gl_profil.setHorizontalGroup(
			gl_profil.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_profil.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_profil.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_profil.createSequentialGroup()
							.addComponent(label_6)
							.addContainerGap(616, Short.MAX_VALUE))
						.addGroup(Alignment.TRAILING, gl_profil.createSequentialGroup()
							.addGroup(gl_profil.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_profil.createSequentialGroup()
									.addComponent(btnNewButton_3, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, 406, Short.MAX_VALUE)
									.addComponent(btnCoursesManagment))
								.addGroup(gl_profil.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED, 215, Short.MAX_VALUE)
									.addComponent(corrdonnePanel1, GroupLayout.PREFERRED_SIZE, 421, GroupLayout.PREFERRED_SIZE)))
							.addGap(36))))
		);
		gl_profil.setVerticalGroup(
			gl_profil.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_profil.createSequentialGroup()
					.addGap(11)
					.addGroup(gl_profil.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_profil.createSequentialGroup()
							.addGap(12)
							.addComponent(label_6))
						.addComponent(corrdonnePanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
					.addGroup(gl_profil.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnCoursesManagment)
						.addComponent(btnNewButton_3, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		GroupLayout gl_corrdonnePanel1 = new GroupLayout(corrdonnePanel1);
		gl_corrdonnePanel1.setHorizontalGroup(
			gl_corrdonnePanel1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_corrdonnePanel1.createSequentialGroup()
					.addGap(39)
					.addComponent(label_5, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
					.addGap(80)
					.addComponent(firstNameDynamic1, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_corrdonnePanel1.createSequentialGroup()
					.addGap(39)
					.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
					.addGap(80)
					.addComponent(emailDynamic1, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_corrdonnePanel1.createSequentialGroup()
					.addGap(277)
					.addComponent(button_1, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_corrdonnePanel1.createSequentialGroup()
					.addGap(39)
					.addGroup(gl_corrdonnePanel1.createParallelGroup(Alignment.LEADING)
						.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_2))
					.addGap(88)
					.addGroup(gl_corrdonnePanel1.createParallelGroup(Alignment.TRAILING)
						.addComponent(dateOfBirthDynamic1, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
						.addComponent(sexeDynamic1, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)))
				.addGroup(gl_corrdonnePanel1.createSequentialGroup()
					.addGap(39)
					.addGroup(gl_corrdonnePanel1.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(label, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(label_4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(95)
					.addGroup(gl_corrdonnePanel1.createParallelGroup(Alignment.TRAILING)
						.addComponent(lastNameDynamic1, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
						.addComponent(cinDynamic1, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)))
		);
		gl_corrdonnePanel1.setVerticalGroup(
			gl_corrdonnePanel1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_corrdonnePanel1.createSequentialGroup()
					.addGap(65)
					.addGroup(gl_corrdonnePanel1.createParallelGroup(Alignment.LEADING)
						.addComponent(firstNameDynamic1)
						.addComponent(label_5))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_corrdonnePanel1.createParallelGroup(Alignment.LEADING)
						.addComponent(label_4)
						.addComponent(lastNameDynamic1))
					.addGap(6)
					.addGroup(gl_corrdonnePanel1.createParallelGroup(Alignment.LEADING)
						.addComponent(cinDynamic1)
						.addComponent(label))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_corrdonnePanel1.createParallelGroup(Alignment.LEADING)
						.addComponent(dateOfBirthDynamic1)
						.addComponent(label_1))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_corrdonnePanel1.createParallelGroup(Alignment.LEADING)
						.addComponent(sexeDynamic1)
						.addComponent(label_2))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_corrdonnePanel1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_corrdonnePanel1.createSequentialGroup()
							.addComponent(emailDynamic1)
							.addGap(107)
							.addComponent(button_1))
						.addComponent(label_3)))
		);
		corrdonnePanel1.setLayout(gl_corrdonnePanel1);
		profil.setLayout(gl_profil);
		
		JLayeredPane quiz = new JLayeredPane();
		TeacherProfil.addTab("Quiz Management", null, quiz, null);
		
		JPanel quizPanel = new JPanel();
		quizPanel.setBorder(new TitledBorder(null, "Quiz", TitledBorder.LEFT, TitledBorder.TOP, null, Color.BLUE));
		quizPanel.setBounds(10, 11, 652, 213);
		quiz.add(quizPanel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent e) {
				
			}
		});
		GroupLayout gl_quizPanel = new GroupLayout(quizPanel);
		gl_quizPanel.setHorizontalGroup(
			gl_quizPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_quizPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 624, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_quizPanel.setVerticalGroup(
			gl_quizPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_quizPanel.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 191, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		tQuiz = new JTable();
		tQuiz.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				btnDeleteQuiz.setEnabled(true);
				quizSelected = quizs.get(tQuiz.getSelectedRow());
				tfNameQuiz.setText(" "+quizSelected.getNameQuiz());
				
			}
		});
		scrollPane.setViewportView(tQuiz);
		quizPanel.setLayout(gl_quizPanel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Manage Quiz", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLUE));
		panel_1.setBounds(10, 224, 652, 143);
		quiz.add(panel_1);
		
		JButton btnManageQuestion = new JButton("Manage Question");
		btnManageQuestion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				QuestionManagementFrame frame = new QuestionManagementFrame();
				frame.setVisible(true);
				setVisible(false);
			}
		});
		
		btnUpdateQuiz = new JButton("Update Quiz ");
		btnUpdateQuiz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				quizSelected.setNameQuiz(tfNameQuiz.getText());
				ExamManagementDelegate.updateQuiz(quizSelected);
				quizs = ExamManagementDelegate.findAllQuiz();
				initDataBindings();
				clear();
			}
		});
		
		btnDeleteQuiz = new JButton("Delete Quiz");
		btnDeleteQuiz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				quizSelected=quizs.get(tQuiz.getSelectedRow()); 
				ExamManagementDelegate.deleteQuiz(quizSelected);
				btnDeleteQuiz.setEnabled(false);
				quizs=ExamManagementDelegate.findAllQuiz();
				initDataBindings();
				clear();
			}
		});
		
		tfNameQuiz = new JTextField();
		tfNameQuiz.setColumns(10);
		
		JLabel lblQuiz = new JLabel("Quiz :");
		
		JButton btnClear = new JButton("clear");
		
		
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				quizSelected = new Quiz();
				clear();
			}
		});
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(26)
					.addComponent(lblQuiz)
					.addGap(50)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(tfNameQuiz, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnClear))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addComponent(btnDeleteQuiz)
							.addContainerGap())
						.addGroup(gl_panel_1.createSequentialGroup()
							.addComponent(btnUpdateQuiz, GroupLayout.PREFERRED_SIZE, 89, Short.MAX_VALUE)
							.addGap(158)
							.addComponent(btnManageQuestion)
							.addGap(45))))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnManageQuestion)
						.addComponent(tfNameQuiz, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblQuiz)
						.addComponent(btnUpdateQuiz))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnDeleteQuiz)
						.addComponent(btnClear))
					.addContainerGap(40, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		contentPane.setLayout(gl_contentPane);
		initDataBindings();
	}
	protected void initDataBindings() {
		JTableBinding<Quiz, List<Quiz>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE, quizs, tQuiz);
		//
		BeanProperty<Quiz, String> quizBeanProperty = BeanProperty.create("nameQuiz");
		jTableBinding.addColumnBinding(quizBeanProperty).setColumnName("Title of Quiz");
		//
		jTableBinding.bind();
	}
	private void clear() {
		tfNameQuiz.setText("");
	}
}
