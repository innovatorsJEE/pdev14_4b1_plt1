package client.ui;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import tn.edu.pdev.moocs.domain.Student;
import tn.edu.pdev.moocs.domain.Teacher;
import tn.edu.pdev.moocs.domain.User;

import client.delegate.profilsManagement.ProfilsManagementDelegate;


public class UserTablesModel extends AbstractTableModel{

	
	
	 String[] tabColonnes = {"First Name","Last Name","CIN","Date Of Birth","Sexe","Mail","Login","Password","Activated"};
	 List<Teacher> tabLignes = ProfilsManagementDelegate.findAllTeacher();
	
	
	
	 public UserTablesModel() {

	 }
	 
	
	public int getRowCount() {
		return tabLignes.size();
	}

	public int getColumnCount() {
		return tabColonnes.length;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		
		switch(columnIndex){
		
		case 0:
			return tabLignes.get(rowIndex).getId();
		case 1:	
			return tabLignes.get(rowIndex).getLastName();
		case 2:
			return tabLignes.get(rowIndex).getCin();
		case 3:
			return tabLignes.get(rowIndex).getDateOfBirth();
		case 4:
			return tabLignes.get(rowIndex).getSexe();
		case 5:
			return tabLignes.get(rowIndex).getMail();
		case 6:
			return tabLignes.get(rowIndex).getLogin();
		case 7:
			return tabLignes.get(rowIndex).getPassword();
		case 8:
			return tabLignes.get(rowIndex).isAct();
		
		default:return null;
		
		}
		
		
		
		
	}

    public String getColumnName(int i) {
        return tabColonnes[i];
    }


}
