package client.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.LayoutStyle.ComponentPlacement;

import client.delegate.courseManagement.CouseManagementDelegate;
import client.sessions.SessionTeacher;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.JTextField;
import javax.swing.JRadioButton;

import tn.edu.pdev.moocs.domain.Chapter;
import tn.edu.pdev.moocs.domain.Course;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.ObjectProperty;
import org.jdesktop.beansbinding.BeanProperty;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ChaptersFrame extends JFrame {

	private JPanel contentPane;
	private JTextField title;
	List<Chapter>chapters;
	Chapter chapter;
	Chapter chapterSelected;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChaptersFrame frame = new ChaptersFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ChaptersFrame() {
		chapters = CouseManagementDelegate.findChaptersByCourse(SessionTeacher.getInstance().getCourse());
		setTitle("Chapters");
		setVisible(true);
		setBounds(100, 100, 448, 560);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.windowBorder);
		
		JPanel panel_1 = new JPanel();
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Chapters Managment", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(160, 82, 45)));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 422, Short.MAX_VALUE)
				.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 422, Short.MAX_VALUE)
				.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 422, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 354, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE))
		);
		
		JLabel lblTitle = new JLabel("Title");
		
		title = new JTextField();
		title.setColumns(10);
		
		
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(title.getText().equals("")){
					new Alert4Frame();
				}else if(SessionTeacher.getInstance().getChapter()== null){
					new Alert4Frame();
				}else{
				chapterSelected.setTitle(title.getText());
				CouseManagementDelegate.updateChapter(chapterSelected);
				title.setText("");
				chapters= CouseManagementDelegate.findChaptersByCourse(SessionTeacher.getInstance().getCourse());
				initDataBindings();}
			}
		});
		
		JButton btnNew = new JButton("New ");
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(title.getText().equals("")){
					new Alert5Frame();
				}else{
				chapter = new Chapter(title.getText()/*, true*/);
				chapter.setCourse(SessionTeacher.getInstance().getCourse());
				chapter = CouseManagementDelegate.createChapter(chapter);
				title.setText("");
			}
				}
		});
		
		JButton btnFinsh = new JButton("Finsh");
		btnFinsh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addGap(22)
							.addComponent(lblTitle)
							.addGap(18)
							.addComponent(title, GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addContainerGap(195, Short.MAX_VALUE)
							.addComponent(btnUpdate)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNew)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnFinsh)
							.addGap(12)))
					.addContainerGap())
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTitle)
						.addComponent(title, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnUpdate)
						.addComponent(btnNew)
						.addComponent(btnFinsh)))
		);
		panel_2.setLayout(gl_panel_2);
		
		JPanel panel_3 = new JPanel();
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(SessionTeacher.getInstance().getChapter()== null){
					new Alert4Frame();
				}else {
					CouseManagementDelegate.deleteChapter(SessionTeacher.getInstance().getChapter());
					chapters = CouseManagementDelegate.findChaptersByCourse(SessionTeacher.getInstance().getCourse());
					initDataBindings();
					title.setText("");
					
				}
			}
		});
		
		JButton btnAddContain = new JButton("Add Contain");
		btnAddContain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(chapterSelected == null){
					new Alert4Frame();
				}else{
					
					new ContainFrame();
					setVisible(false);
				}
			}
		});
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addComponent(panel_3, GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(Alignment.TRAILING, gl_panel_1.createSequentialGroup()
							.addComponent(btnDelete)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnAddContain)
							.addGap(9))))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 301, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnDelete)
						.addComponent(btnAddContain))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_panel_3 = new GroupLayout(panel_3);
		gl_panel_3.setHorizontalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_3.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panel_3.setVerticalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_3.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				chapterSelected=chapters.get(table.getSelectedRow());			   
				title.setText(chapterSelected.getTitle());
				SessionTeacher.getInstance().setChapter(chapterSelected);
			}
		});
		scrollPane.setViewportView(table);
		panel_3.setLayout(gl_panel_3);
		panel_1.setLayout(gl_panel_1);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(ChaptersFrame.class.getResource("/client/ui/menu_arrow.gif")));
		
		JLabel lblAaaaaaa = new JLabel(SessionTeacher.getInstance().getThematic().getNameThematic());
		lblAaaaaaa.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAaaaaaa.setForeground(SystemColor.window);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(ChaptersFrame.class.getResource("/client/ui/menu_arrow.gif")));
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(ChaptersFrame.class.getResource("/client/ui/menu_arrow.gif")));
		
		JLabel lblBbbbb = new JLabel(SessionTeacher.getInstance().getCourse().getCourseName());
		lblBbbbb.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblBbbbb.setForeground(SystemColor.window);
		
		JButton button = new JButton("");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chapters = CouseManagementDelegate.findChaptersByCourse(SessionTeacher.getInstance().getCourse());
				initDataBindings();
				
			}
		});
		button.setIcon(new ImageIcon(ChaptersFrame.class.getResource("/client/ui/ref2.png")));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(label)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAaaaaaa)
					.addGap(42)
					.addComponent(label_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNewLabel)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblBbbbb)
					.addPreferredGap(ComponentPlacement.RELATED, 84, Short.MAX_VALUE)
					.addComponent(button, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(label)
						.addComponent(lblAaaaaaa)
						.addComponent(label_1)
						.addComponent(lblNewLabel)
						.addComponent(lblBbbbb)
						.addComponent(button, GroupLayout.PREFERRED_SIZE, 22, Short.MAX_VALUE))
					.addContainerGap())
		);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
		initDataBindings();
	}
	protected void initDataBindings() {
		JTableBinding<Chapter, List<Chapter>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE, chapters, table);
		//
		BeanProperty<Chapter, String> chapterBeanProperty = BeanProperty.create("title");
		jTableBinding.addColumnBinding(chapterBeanProperty).setColumnName("Chapters");
		//
		jTableBinding.bind();
	}
}
