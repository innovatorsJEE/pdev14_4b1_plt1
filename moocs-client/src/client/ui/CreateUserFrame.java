package client.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.SystemColor;
import java.awt.Font;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import tn.edu.pdev.moocs.domain.Student;
import tn.edu.pdev.moocs.domain.Teacher;
import tn.edu.pdev.moocs.domain.User;

import client.delegate.profilsManagement.ProfilsManagementDelegate;

import client.sessions.Session;

import com.toedter.calendar.JDateChooser;
import javax.swing.ButtonGroup;

public class CreateUserFrame  extends JFrame implements ActionListener{

	private JPanel contentPane;
	private JTextField tf_fn;
	private JTextField tf_ln;
	private JTextField tf_cin;
	private JTextField tf_log;
	private JDateChooser dof;
	private JRadioButton br;
	private JLabel lblLogin ;
	private JButton btnNewButton ;
	public JRadioButton br2;
    int i =0;
   private JTextField tf_mail;
   private JTextField tf_dip;
   private JLabel lblDiploma;
   private final ButtonGroup buttonGroup = new ButtonGroup();
   private JPasswordField tf_pass;
   private JTextField tf_edu;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateUserFrame frame = new CreateUserFrame();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CreateUserFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 560, 630);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setToolTipText("\r\n");
		panel.setBorder(new EmptyBorder(0, 0, 0, 0));
		panel.setBackground(Color.WHITE);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 514, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 584, Short.MAX_VALUE)
		);
		
		JLabel lblNewLabel = new JLabel("First Name \r\n");
		lblNewLabel.setFont(new Font("Stencil", Font.PLAIN, 16));
		lblNewLabel.setForeground(Color.BLUE);
		
		tf_fn = new JTextField();
		tf_fn.setBackground(new Color(255, 255, 240));
		tf_fn.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Last Name");
		lblNewLabel_1.setForeground(Color.BLUE);
		lblNewLabel_1.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		tf_ln = new JTextField();
		tf_ln.setColumns(10);
		tf_ln.setBackground(new Color(255, 255, 240));
		
		JLabel lblCin = new JLabel("CIN / Passport");
		lblCin.setForeground(Color.BLUE);
		lblCin.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		tf_cin = new JTextField();
		tf_cin.setColumns(10);
		tf_cin.setBackground(new Color(255, 255, 240));
		
		JLabel lblDateOfBirth = new JLabel("Date of birth");
		lblDateOfBirth.setForeground(Color.BLUE);
		lblDateOfBirth.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		JLabel lblSexe = new JLabel("Sexe");
		lblSexe.setForeground(Color.BLUE);
		lblSexe.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		final JComboBox cb_sexe = new JComboBox();
		cb_sexe.setFont(new Font("Georgia", Font.BOLD, 11));
		cb_sexe.setModel(new DefaultComboBoxModel(new String[] {"Man","Women"}));
		cb_sexe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		cb_sexe.setToolTipText("Man");
		
		JLabel lblMail = new JLabel("Mail\r\n");
		lblMail.setForeground(Color.BLUE);
		lblMail.setFont(new Font("Stencil", Font.PLAIN, 16));
		lblLogin = new JLabel("Education");
		lblLogin.setForeground(Color.BLUE);
		lblLogin.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		
		tf_log = new JTextField();
		tf_log.setColumns(10);
		tf_log.setBackground(new Color(255, 255, 240));
		
		
		JLabel lblPassword = new JLabel("login\r\n");
		lblPassword.setForeground(Color.BLUE);
		lblPassword.setFont(new Font("Stencil", Font.PLAIN, 16));
		

		tf_dip = new JTextField();
		tf_dip.setBackground(new Color(255, 255, 240));
		tf_dip.setEnabled(false);
		//tf_dip.setBackground(UIManager.getColor("Button.highlight"));
		tf_dip.setColumns(10);
		
		JLabel lblPassword_1 = new JLabel("password");
		lblPassword_1.setForeground(Color.BLUE);
		lblPassword_1.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		btnNewButton = new JButton("Create My MOOCs Account");
		btnNewButton.setEnabled(false);
		btnNewButton.setForeground(SystemColor.inactiveCaptionText);
		btnNewButton.setFont(new Font("Elephant", Font.PLAIN, 12));
		
		 dof = new JDateChooser();
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AccuFrame accuFrame = new AccuFrame();
				accuFrame.setVisible(true);
				setVisible(false);
			}
		});
	
		btnBack.setForeground(Color.WHITE);
		btnBack.setBackground(new Color(135, 206, 235));
		
		 br = new JRadioButton("Student");
		 br.setFont(new Font("Elephant", Font.PLAIN, 12));
		 buttonGroup.add(br);
	
		br.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
			//	 JOptionPane.showMessageDialog(null,"CIN IS NUMBER!!","WORRING",JOptionPane.ERROR_MESSAGE);
				
			}
		});
		br.addActionListener(this);
		
//		if(br.isEnabled())
//		{
//		
//			
//		}
//		if(br.isSelected()==false)
//		{
//		tf_edu.setEnabled(false);
//		}
//		
		tf_mail = new JTextField();
		tf_mail.setBackground(new Color(255, 255, 240));
		tf_mail.setColumns(10);
		
		lblDiploma = new JLabel("Diploma");
		lblDiploma.setForeground(Color.BLUE);
		lblDiploma.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		
		
		final JRadioButton br2 = new JRadioButton("Teacher");
		br2.setFont(new Font("Elephant", Font.PLAIN, 12));
		buttonGroup.add(br2);
	
		br2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			//	 JOptionPane.showMessageDialog(null,"CIN IS NUMBER!!","WORRING",JOptionPane.ERROR_MESSAGE);
				 
				if(br2.isSelected())
				{
					btnNewButton.setEnabled(true);
				 lblDiploma.setVisible(true);
			//	 tf_dip.is
				 tf_dip.setEnabled(true);
				 tf_dip.setBackground(new Color(175, 238, 238));
				 tf_edu.setBackground(new Color(255, 255, 255));
				 tf_edu.setText("");
				}
				if(!br2.isSelected())
				{
				lblDiploma.setVisible(false);
				tf_dip.setEnabled(false);
				tf_dip.setBackground(UIManager.getColor("Button.background"));
				tf_edu.setBackground(new Color(175, 238, 238));
				
				}
			}
		});
		
		br2.addActionListener(this);
		
		tf_pass = new JPasswordField();
		tf_pass.setBackground(new Color(255, 255, 240));
		
		tf_edu = new JTextField();
		tf_edu.setBackground(new Color(255, 255, 240));
		//tf_edu.setBackground(new Color(255, 255, 255));
		tf_edu.setEnabled(false);
		tf_edu.setColumns(10);
	
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				try
				{
				User userr=null;
				Date ddn = dof.getDate();
				DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRANCE);
				
				
				String fn = tf_fn.getText();
				String ln = tf_ln.getText();
				Long cin = Long.parseLong(tf_cin.getText());
				
				String sexe = cb_sexe.getSelectedItem().toString();
				String mail = tf_mail.getText();
				String log = tf_log.getText();
				String pass = tf_pass.getText();
				
				String edu = tf_edu.getText();
				String dip = tf_dip.getText();
          
				

				if(br.isSelected())
				{
					if(fn.equals("") || ln.equals("") || mail.equals("") || log.equals("") || pass.equals("")||edu.equals(""))
					{
						JOptionPane.showMessageDialog(null,"Input ERROR !!","Probl�me de format",JOptionPane.ERROR_MESSAGE);	
					}
					else
					{
					userr = new Student(fn, ln, cin, ddn, sexe, mail, log, pass,edu);
					userr = ProfilsManagementDelegate.createUser(userr);
					System.out.println(userr.isAct());
					Session.getInstance().setUser(userr);
					
					StudentAccountFrame frame = new StudentAccountFrame();
					frame.setVisible(true);
					setVisible(false);
					}
				}
				if(br2.isSelected())
				{
					if(fn.equals("") || ln.equals("") || mail.equals("") || log.equals("") || pass.equals("")||dip.equals(""))
					{
						JOptionPane.showMessageDialog(null,"Input ERROR !!","Probl�me de format",JOptionPane.ERROR_MESSAGE);	
					}
					else
					{				
					
				userr= new Teacher(fn, ln, cin, ddn, sexe, mail, log, pass,dip);	
				userr = ProfilsManagementDelegate.createUser(userr);
				System.out.println(userr.isAct());
				Session.getInstance().setUser(userr);
				
				AccuFrame accuFrame= new AccuFrame();
				accuFrame.setVisible(true);
				setVisible(false);
				
				
					}
				
				}
				
				}
				catch(NumberFormatException exception)
				{
					JOptionPane.showMessageDialog(null,"CIN is Number !","Probl�me de format",JOptionPane.ERROR_MESSAGE);
				}
			
	         catch (NullPointerException npe) {
	            JOptionPane.showMessageDialog(null, "Input ERROR !!", "Probl�me de format", JOptionPane.ERROR_MESSAGE);
	          }

						
				
				
				
				
			}
		});
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(21)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblDiploma)
							.addContainerGap())
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_panel.createSequentialGroup()
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
											.addComponent(lblSexe, GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
											.addComponent(lblCin, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
											.addComponent(lblNewLabel))
										.addGap(39))
									.addGroup(gl_panel.createSequentialGroup()
										.addComponent(lblDateOfBirth, GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
										.addPreferredGap(ComponentPlacement.RELATED)))
								.addGroup(gl_panel.createSequentialGroup()
									.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
										.addComponent(lblMail, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblPassword, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblPassword_1, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE))
									.addGap(29))
								.addGroup(gl_panel.createSequentialGroup()
									.addComponent(lblLogin, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
									.addGap(29)))
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(tf_fn, GroupLayout.PREFERRED_SIZE, 313, GroupLayout.PREFERRED_SIZE)
								.addComponent(tf_ln, GroupLayout.PREFERRED_SIZE, 313, GroupLayout.PREFERRED_SIZE)
								.addComponent(tf_cin, GroupLayout.PREFERRED_SIZE, 313, GroupLayout.PREFERRED_SIZE)
								.addComponent(dof, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(cb_sexe, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
								.addComponent(tf_mail, GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE)
								.addComponent(tf_log, GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE)
								.addComponent(tf_pass, GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE)
								.addComponent(tf_edu, GroupLayout.DEFAULT_SIZE, 331, Short.MAX_VALUE)
								.addComponent(tf_dip, GroupLayout.DEFAULT_SIZE, 331, Short.MAX_VALUE))
							.addGap(48))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(183)
							.addComponent(br)
							.addGap(18)
							.addComponent(br2)
							.addGap(169))))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(226)
					.addComponent(btnBack)
					.addContainerGap(258, Short.MAX_VALUE))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(100)
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 333, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(106, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(tf_fn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(20)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(tf_ln, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(20)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCin)
						.addComponent(tf_cin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblDateOfBirth)
						.addComponent(dof, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSexe)
						.addComponent(cb_sexe, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(27)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblMail)
						.addComponent(tf_mail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPassword)
						.addComponent(tf_log, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(26)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPassword_1, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(tf_pass, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(29)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(br)
						.addComponent(br2))
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLogin)
						.addComponent(tf_edu, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(26)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDiploma)
						.addComponent(tf_dip, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(34)
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
					.addGap(27)
					.addComponent(btnBack)
					.addContainerGap())
		);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
	
		if(br.isSelected()==true)
		{
//			tf_edu.setBackground(UIManager.getColor("Button.background"));
			tf_dip.setBackground(UIManager.getColor("Button.background"));
			tf_edu.setEnabled(true);
//			tf_edu.addActionListener(this);
			btnNewButton.setEnabled(true);
			tf_edu.setBackground(new Color(175, 238, 238));
			tf_dip.setText("");
			
			//tf_edu.setVisible(true);
			lblLogin.setVisible(true);
			tf_dip.setEnabled(false);
			// JOptionPane.showMessageDialog(null,"CIN IS NUMBER!!","WORRING",JOptionPane.ERROR_MESSAGE);
		}
		else
		{
			tf_edu.setEnabled(false);
			tf_edu.setText("");
		//    tf_edu.setBackground(UIManager.getColor("Button.background"));
			tf_dip.setBackground(new Color(175, 238, 238));
			//tf_edu.setVisible(false);
		//	lblLogin.setVisible(false);
			//btnNewButton.setEnabled(false);
		}
//		if(br2.isSelected()==true)
//		{
//			   JOptionPane.showMessageDialog(null,"CIN IS NUMBER!!","WORRING",JOptionPane.ERROR_MESSAGE);
//			   
//		}
//		else
//		{
//			JOptionPane.showMessageDialog(null,"CIN IS NUMBER!!","WORRING",JOptionPane.ERROR_MESSAGE);
//		}
		
	}
}
