package client.ui ;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Color;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JLayeredPane;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import tn.edu.pdev.moocs.domain.Chapter;
import tn.edu.pdev.moocs.domain.Course;
import tn.edu.pdev.moocs.domain.Thematic;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.ObjectProperty;
import org.jdesktop.beansbinding.BeanProperty;

import client.delegate.courseManagement.CouseManagementDelegate;
import client.sessions.SessionTeacher;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class StudentAccountFrame1 extends JFrame {

	private JPanel contentPane;
	private JTable tThematics;
	private JTable tCourses;
	List <Thematic> thematics;
	Thematic thematic = new Thematic();
	List<Course> courses;
	Course course = new Course();
	Thematic thematicSelected ;
	Thematic t ;
	Course courseSelected ;
	List<Chapter> chapters ;
	Chapter chapter = new Chapter() ;
	Course c ;
	Chapter chapterSelected;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StudentAccountFrame1 frame = new StudentAccountFrame1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StudentAccountFrame1() {
		setVisible(true);
		
		thematics = CouseManagementDelegate.findAllThematic();
		
		
		
		thematics = CouseManagementDelegate.findAllThematic();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 652, 551);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JTabbedPane ProfilStudent = new JTabbedPane(JTabbedPane.TOP);
		ProfilStudent.setBackground(new Color(255, 255, 255));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(ProfilStudent, GroupLayout.PREFERRED_SIZE, 633, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(ProfilStudent, GroupLayout.DEFAULT_SIZE, 502, Short.MAX_VALUE)
		);
		
		JLayeredPane coursesTab = new JLayeredPane();
		coursesTab.setBackground(new Color(205, 133, 63));
		ProfilStudent.addTab("Courses", null, coursesTab, null);
		
		JPanel panel = new JPanel();
		panel.setForeground(new Color(0, 0, 205));
		panel.setBackground(new Color(255, 255, 224));
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Thematics", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(128, 0, 0)));
		panel.setBounds(10, 11, 608, 201);
		coursesTab.add(panel);
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 579, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		tThematics = new JTable();
		tThematics.setBackground(new Color(238, 232, 170));
		tThematics.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				thematicSelected=thematics.get(tThematics.getSelectedRow());			   

			}
		});
		scrollPane.setViewportView(tThematics);
		panel.setLayout(gl_panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setForeground(new Color(0, 0, 139));
		panel_1.setBackground(new Color(255, 255, 224));
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Courses", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(128, 0, 0)));
		panel_1.setBounds(10, 257, 608, 172);
		coursesTab.add(panel_1);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 582, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		tCourses = new JTable();
		tCourses.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				courseSelected=courses.get(tCourses.getSelectedRow());			   
//				courseName.setText(courseSelected.getCourseName());
				SessionTeacher.getInstance().setCourse(courseSelected);
			}
		});
		tCourses.setBackground(new Color(250, 250, 210));
		scrollPane_1.setViewportView(tCourses);
		panel_1.setLayout(gl_panel_1);
		
		JButton btnNewButton = new JButton("Show Courses");
		btnNewButton.setBackground(new Color(250, 250, 210));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				t = CouseManagementDelegate.findThematic(thematicSelected.getId());
				courses = CouseManagementDelegate.findCoursesByThematic(t);
				initDataBindings();
			}
		});
		btnNewButton.setBounds(443, 223, 141, 23);
		coursesTab.add(btnNewButton);
		
		JButton btnShowCourse = new JButton("Show Chapters");
		btnShowCourse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(courseSelected!=null){
				setVisible(false);
				new ShowCourseFrame();}else {
					JOptionPane.showMessageDialog(null,"Please Select a Course!!","WORRING",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnShowCourse.setBounds(446, 440, 138, 23);
		coursesTab.add(btnShowCourse);
		btnShowCourse.setBackground(new Color(250, 250, 210));
		
		JButton btnMyProfile = new JButton("My Profile");
		btnMyProfile.setForeground(new Color(0, 0, 205));
		btnMyProfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new StudentAccountFrame();
				setVisible(false);
			}
		});
		btnMyProfile.setBounds(20, 440, 89, 23);
		coursesTab.add(btnMyProfile);
		contentPane.setLayout(gl_contentPane);
		initDataBindings();
	}
	protected void initDataBindings() {
		JTableBinding<Thematic, List<Thematic>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE, thematics, tThematics);
		//
		BeanProperty<Thematic, String> thematicBeanProperty = BeanProperty.create("nameThematic");
		jTableBinding.addColumnBinding(thematicBeanProperty).setColumnName(".");
		//
		jTableBinding.bind();
		//
		JTableBinding<Course, List<Course>, JTable> jTableBinding_1 = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE, courses, tCourses);
		//
		BeanProperty<Course, String> courseBeanProperty = BeanProperty.create("courseName");
		jTableBinding_1.addColumnBinding(courseBeanProperty).setColumnName(".");
		//
		jTableBinding_1.bind();
	}
}
