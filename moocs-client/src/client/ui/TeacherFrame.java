package client.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Color;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;

import client.delegate.courseManagement.CouseManagementDelegate;
import client.sessions.Session;
import client.sessions.SessionTeacher;

import tn.edu.pdev.moocs.domain.Course;
import tn.edu.pdev.moocs.domain.Teacher;
import tn.edu.pdev.moocs.domain.Thematic;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.ObjectProperty;
import org.jdesktop.beansbinding.BeanProperty;
import javax.swing.JTextField;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.Bindings;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.UIManager;
import javax.swing.ImageIcon;
import java.awt.SystemColor;

public class TeacherFrame extends JFrame {

	private JPanel contentPane;
	List<Thematic>thematics;
	List<Course>courses;
	Thematic thematicSelected ;
	Thematic t;
	Course courseSelected;
	private JTable table;
	private JTextField nameThematic;
	JLabel lblNewLabel_1;
	SessionTeacher sessionTeacher = new SessionTeacher();
	
	JPanel panel_4 = new JPanel();
	JLabel lblNewLabel = new JLabel("");
	JPanel panel_3 = new JPanel();
	JLabel lblAaaa = new JLabel("");
	private JTextField courseName;
	GroupLayout gl_panel_3 = new GroupLayout(panel_3);
	private JTable table_1;
	private JScrollPane scrollPane_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TeacherFrame frame = new TeacherFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TeacherFrame() {
		setTitle("Courses Managment");
		thematics = CouseManagementDelegate.findAllThematic();
		courses = CouseManagementDelegate.findCoursesByThematicAndTheacher(t, (Teacher) Session.getInstance().getUser());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 836, 561);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 801, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 486, Short.MAX_VALUE)
		);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Thematics", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(160, 82, 45)));
		
		
		panel_3.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Courses", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(160, 82, 45)));
		panel_3.setBackground(Color.WHITE);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 278, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_3, GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addComponent(panel_3, GroupLayout.DEFAULT_SIZE, 475, Short.MAX_VALUE)
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addContainerGap())
		);
		
		
		panel_4.setBackground(SystemColor.textInactiveText);
		
		
		lblNewLabel.setIcon(new ImageIcon(TeacherFrame.class.getResource("/client/ui/menu_arrow.gif")));
		
		
		lblAaaa.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblAaaa.setBackground(new Color(245, 222, 179));
		lblAaaa.setForeground(new Color(255, 255, 240));
		GroupLayout gl_panel_4 = new GroupLayout(panel_4);
		gl_panel_4.setHorizontalGroup(
			gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_4.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblAaaa)
					.addContainerGap(389, Short.MAX_VALUE))
		);
		gl_panel_4.setVerticalGroup(
			gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_4.createSequentialGroup()
					.addGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel)
						.addComponent(lblAaaa))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel_4.setLayout(gl_panel_4);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(new Color(245, 222, 179));
		
		JPanel panel_6 = new JPanel();
		panel_6.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Courses Managment", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(160, 82, 45)));
		
		gl_panel_3.setHorizontalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_3.createSequentialGroup()
					.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_3.createSequentialGroup()
							.addGap(10)
							.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
								.addComponent(panel_4, GroupLayout.PREFERRED_SIZE, 475, GroupLayout.PREFERRED_SIZE)
								.addComponent(panel_5, GroupLayout.PREFERRED_SIZE, 475, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_panel_3.createSequentialGroup()
							.addContainerGap()
							.addComponent(panel_6, GroupLayout.DEFAULT_SIZE, 475, Short.MAX_VALUE)))
					.addContainerGap())
		);
		gl_panel_3.setVerticalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_3.createSequentialGroup()
					.addComponent(panel_4, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(panel_5, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(panel_6, GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE))
		);
		
		JButton btnNew_1 = new JButton("New");
		btnNew_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(SessionTeacher.getInstance().getThematic()!= null){
				new NewCourseFrame();}else {
					new Alert1Frame();
				}
			}
		});
		
		scrollPane_1 = new JScrollPane();
		GroupLayout gl_panel_5 = new GroupLayout(panel_5);
		gl_panel_5.setHorizontalGroup(
			gl_panel_5.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_5.createSequentialGroup()
					.addGroup(gl_panel_5.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_5.createSequentialGroup()
							.addGap(412)
							.addComponent(btnNew_1))
						.addGroup(gl_panel_5.createSequentialGroup()
							.addContainerGap()
							.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 455, Short.MAX_VALUE)))
					.addContainerGap())
		);
		gl_panel_5.setVerticalGroup(
			gl_panel_5.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_5.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 219, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNew_1))
		);
		
		table_1 = new JTable();
		table_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				courseSelected=courses.get(table_1.getSelectedRow());			   
				courseName.setText(courseSelected.getCourseName());
				sessionTeacher.getInstance().setCourse(courseSelected);
				
//				thematicSelected=thematics.get(table.getSelectedRow());			   
//				nameThematic.setText(thematicSelected.getNameThematic());
//				sessionTeacher.getInstance().setThematic(thematicSelected);
				

			}
		});
		table_1.setBackground(new Color(255, 228, 181));
		scrollPane_1.setViewportView(table_1);
		panel_5.setLayout(gl_panel_5);
		
		JLabel lblCourseTitle = new JLabel("Course Title");
		
		courseName = new JTextField();
		courseName.setColumns(10);
		
		JButton btnUpdate_1 = new JButton("Update");
		btnUpdate_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(SessionTeacher.getInstance().getCourse()== null){
					new Alert2Frame();
				}else{
				courseSelected.setCourseName(courseName.getText());
				CouseManagementDelegate.updateCourse(courseSelected);
				courseName.setText("");
				courses = CouseManagementDelegate.findCoursesByThematicAndTheacher(t, (Teacher) Session.getInstance().getUser());
				initDataBindings1();
				}
			}
		});
		
		JButton btnDelete_1 = new JButton("Delete");
		btnDelete_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(SessionTeacher.getInstance().getCourse()== null){
					new Alert2Frame();
				}else{
				CouseManagementDelegate.deleteCourse(courseSelected);
				courseName.setText("");
				courses = CouseManagementDelegate.findCoursesByThematicAndTheacher(t, (Teacher) Session.getInstance().getUser());
				initDataBindings1();
				sessionTeacher.getInstance().setCourse(null);
				}
				
			}
		});
		
		JButton btnAddChapter = new JButton("");
		btnAddChapter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(SessionTeacher.getInstance().getThematic()== null){
					new Alert1Frame();
				}else if(SessionTeacher.getInstance().getCourse()== null){
					new Alert2Frame();					
				}else {
					new ChaptersFrame() ;
				}
			}
		});
		btnAddChapter.setIcon(new ImageIcon(TeacherFrame.class.getResource("/client/ui/chptrs.png")));
		GroupLayout gl_panel_6 = new GroupLayout(panel_6);
		gl_panel_6.setHorizontalGroup(
			gl_panel_6.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_6.createSequentialGroup()
					.addGroup(gl_panel_6.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_6.createSequentialGroup()
							.addGap(42)
							.addComponent(btnAddChapter, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
							.addGap(195)
							.addComponent(btnUpdate_1)
							.addGap(18)
							.addComponent(btnDelete_1))
						.addGroup(gl_panel_6.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblCourseTitle)
							.addGap(18)
							.addComponent(courseName, GroupLayout.PREFERRED_SIZE, 336, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(26, Short.MAX_VALUE))
		);
		gl_panel_6.setVerticalGroup(
			gl_panel_6.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_6.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_6.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCourseTitle)
						.addComponent(courseName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panel_6.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_6.createParallelGroup(Alignment.BASELINE)
							.addComponent(btnUpdate_1)
							.addComponent(btnDelete_1))
						.addComponent(btnAddChapter, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(32, Short.MAX_VALUE))
		);
		panel_6.setLayout(gl_panel_6);
		panel_3.setLayout(gl_panel_3);
		
		JButton btnNew = new JButton("New");
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new NewThematicFrame();
			}
		});
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(SessionTeacher.getInstance().getThematic()== null){
					new Alert1Frame();
				}else {
					t = CouseManagementDelegate.findThematic(thematicSelected.getId());
					courses = CouseManagementDelegate.findCoursesByThematic(t);
					if(courses.size()!=0){
						new Alert3Frame();
					}else {
						CouseManagementDelegate.deleteThematic(t);
						thematics=CouseManagementDelegate.findAllThematic();
						initDataBindings();
						initDataBindings1();
						nameThematic.setText("");
						sessionTeacher.getInstance().setThematic(null);
						
					}
				}
				
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Thematics Update", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(160, 82, 45)));
		
		JButton btnShowCourses = new JButton("Courses");
		btnShowCourses.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(SessionTeacher.getInstance().getThematic()== null){
					new Alert1Frame();
				}else{
				
				lblAaaa.setText("");
				lblAaaa.setText(sessionTeacher.getInstance().getThematic().getNameThematic());
				lblAaaa.setFont(new Font("Tahoma", Font.BOLD, 15));
				lblAaaa.setBackground(new Color(245, 222, 179));
				lblAaaa.setForeground(new Color(255, 255, 240));
				GroupLayout gl_panel_4 = new GroupLayout(panel_4);
				gl_panel_4.setHorizontalGroup(
					gl_panel_4.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_4.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblNewLabel)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lblAaaa)
							.addContainerGap(389, Short.MAX_VALUE))
				);
				gl_panel_4.setVerticalGroup(
					gl_panel_4.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_4.createSequentialGroup()
							.addGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel)
								.addComponent(lblAaaa))
							.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
				panel_4.setLayout(gl_panel_4);
				panel_3.setLayout(gl_panel_3);
				
				t = CouseManagementDelegate.findThematic(thematicSelected.getId());
				courses = CouseManagementDelegate.findCoursesByThematicAndTheacher(t, (Teacher) Session.getInstance().getUser());
				initDataBindings1();
				sessionTeacher.getInstance().setCourse(null);
				
				}
			}
		});
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon(TeacherFrame.class.getResource("/client/ui/538680-refresh_256x256.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				thematics= CouseManagementDelegate.findAllThematic();
				courses = CouseManagementDelegate.findCoursesByThematicAndTheacher(t, (Teacher) Session.getInstance().getUser());
				initDataBindings();
				initDataBindings1();
				sessionTeacher.getInstance().setCourse(null);
			}
		});
		
		JButton button = new JButton("");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sessionTeacher.getInstance().setThematic(null);
				sessionTeacher.getInstance().setCourse(null);
				TeacherAccountFrame accountFrame = new TeacherAccountFrame();
				accountFrame.setVisible(true);
				setVisible(false);
			}
		});
		button.setIcon(new ImageIcon(TeacherFrame.class.getResource("/client/ui/prof.png")));
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnNew)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnDelete)
							.addPreferredGap(ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
							.addComponent(button, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap(210, Short.MAX_VALUE)
							.addComponent(btnShowCourses))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(6)
							.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)))
					.addContainerGap())
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnDelete)
								.addComponent(btnNew)))
						.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
						.addComponent(button, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 167, GroupLayout.PREFERRED_SIZE)
					.addGap(31)
					.addComponent(btnShowCourses)
					.addGap(18)
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE)
					.addGap(22))
		);
		
		JLabel lblThematicName = new JLabel("Thematic Name");
		
		nameThematic = new JTextField();
		nameThematic.setColumns(10);
		
		JButton btnUpdate = new JButton("Change");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(SessionTeacher.getInstance().getThematic()== null){
					new Alert1Frame();
				}else{
				thematicSelected.setNameThematic(nameThematic.getText());
				CouseManagementDelegate.updateThematic(thematicSelected);
				nameThematic.setText("");
				thematics= CouseManagementDelegate.findAllThematic();
				initDataBindings();
				}
			}
		});
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(nameThematic, GroupLayout.DEFAULT_SIZE, 222, Short.MAX_VALUE)
						.addComponent(lblThematicName)
						.addComponent(btnUpdate, Alignment.TRAILING))
					.addContainerGap())
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(30)
					.addComponent(lblThematicName)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(nameThematic, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnUpdate)
					.addContainerGap(35, Short.MAX_VALUE))
		);
		panel_2.setLayout(gl_panel_2);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				thematicSelected=thematics.get(table.getSelectedRow());			   
				nameThematic.setText(thematicSelected.getNameThematic());
				sessionTeacher.getInstance().setThematic(thematicSelected);
				
			}
			@Override
			public void mousePressed(MouseEvent e) {
			}
		});
		table.setBackground(new Color(253, 245, 230));
		scrollPane.setViewportView(table);
		panel_1.setLayout(gl_panel_1);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
		initDataBindings();
	}
	protected void initDataBindings() {
		JTableBinding<Thematic, List<Thematic>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE, thematics, table);
		//
		BeanProperty<Thematic, String> thematicBeanProperty = BeanProperty.create("nameThematic");
		jTableBinding.addColumnBinding(thematicBeanProperty).setColumnName("Thematics");
		//
		jTableBinding.bind();

	}
	protected void initDataBindings1() {
		//
		JTableBinding<Course, List<Course>, JTable> jTableBinding_1 = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE, courses, table_1);
		//
		BeanProperty<Course, String> courseBeanProperty = BeanProperty.create("courseName");
		jTableBinding_1.addColumnBinding(courseBeanProperty).setColumnName("Courses");
		//
		jTableBinding_1.bind();
		
	}
}
