package client.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Color;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenu;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTabbedPane;
import javax.swing.JLayeredPane;
import javax.swing.SwingConstants;
import javax.swing.JButton;

import tn.edu.pdev.moocs.domain.User;

import client.sessions.Session;

public class StudentAccountFrame extends JFrame {

	private JPanel contentPane;
	
	//User user = Session.getInstance().getUser();
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StudentAccountFrame frame = new StudentAccountFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StudentAccountFrame() {
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 652, 450);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JTabbedPane ProfilStudent = new JTabbedPane(JTabbedPane.TOP);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(ProfilStudent, GroupLayout.PREFERRED_SIZE, 633, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(51, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(ProfilStudent, GroupLayout.PREFERRED_SIZE, 401, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		JLayeredPane profilTab = new JLayeredPane();
		ProfilStudent.addTab("Profil", null, profilTab, null);
		
		JPanel coordonneePanel = new JPanel();
		coordonneePanel.setBounds(173, 11, 421, 329);
		profilTab.add(coordonneePanel);
		
		JLabel cin = new JLabel("CIN :");
		cin.setForeground(Color.BLUE);
		
		JLabel dateofbirth = new JLabel("date of birth :");
		dateofbirth.setForeground(Color.BLUE);
		
		JLabel sexe = new JLabel(" Sexe :");
		sexe.setForeground(Color.BLUE);
		
		JLabel email = new JLabel("E-mail :");
		email.setForeground(Color.BLUE);
		
		JLabel lastname = new JLabel("Last Name : ");
		lastname.setForeground(Color.BLUE);
		
		JLabel firstname = new JLabel("First Name : ");
		firstname.setForeground(Color.BLUE);
		
		JButton button = new JButton("Edit Profil");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				StudentUpdate studentUpdate = new  StudentUpdate();
				studentUpdate.setVisible(true);
				setVisible(false);
				
				
				
			}
		});
		
		JLabel cinDynamic = new JLabel(" "+Session.getInstance().getUser().getCin());
		
		JLabel lastNameDynamic = new JLabel(" "+Session.getInstance().getUser().getLastName());
		
		JLabel dateDynamic = new JLabel(" "+Session.getInstance().getUser().getDateOfBirth());
		
		JLabel firstNameDynamic = new JLabel(" "+Session.getInstance().getUser().getFirstName());
		
		JLabel sexeDynamic = new JLabel(" "+Session.getInstance().getUser().getSexe());
		
		JLabel emailDynamic = new JLabel(" "+Session.getInstance().getUser().getMail());
		GroupLayout gl_coordonneePanel = new GroupLayout(coordonneePanel);
		gl_coordonneePanel.setHorizontalGroup(
			gl_coordonneePanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_coordonneePanel.createSequentialGroup()
					.addGap(39)
					.addGroup(gl_coordonneePanel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(cin, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
						.addComponent(firstname, GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
						.addComponent(lastname, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(dateofbirth, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(sexe, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(email, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED, 77, Short.MAX_VALUE)
					.addGroup(gl_coordonneePanel.createParallelGroup(Alignment.LEADING)
						.addComponent(lastNameDynamic, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
						.addComponent(dateDynamic, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
						.addComponent(cinDynamic, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
						.addComponent(firstNameDynamic, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
						.addComponent(sexeDynamic, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
						.addComponent(emailDynamic, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(104, Short.MAX_VALUE))
				.addGroup(gl_coordonneePanel.createSequentialGroup()
					.addContainerGap(277, Short.MAX_VALUE)
					.addComponent(button, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
					.addGap(38))
		);
		gl_coordonneePanel.setVerticalGroup(
			gl_coordonneePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_coordonneePanel.createSequentialGroup()
					.addGap(65)
					.addGroup(gl_coordonneePanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(firstname)
						.addComponent(firstNameDynamic))
					.addGap(6)
					.addGroup(gl_coordonneePanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lastname)
						.addComponent(lastNameDynamic))
					.addGap(6)
					.addGroup(gl_coordonneePanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(cin)
						.addComponent(cinDynamic))
					.addGap(6)
					.addGroup(gl_coordonneePanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(dateofbirth)
						.addComponent(dateDynamic))
					.addGap(6)
					.addGroup(gl_coordonneePanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(sexe)
						.addComponent(sexeDynamic))
					.addGap(6)
					.addGroup(gl_coordonneePanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(email)
						.addComponent(emailDynamic))
					.addPreferredGap(ComponentPlacement.RELATED, 107, Short.MAX_VALUE)
					.addComponent(button)
					.addGap(20))
		);
		coordonneePanel.setLayout(gl_coordonneePanel);
		
		JButton btnNewButton = new JButton("Disconnect");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AccuFrame accuFrame = new AccuFrame();
				accuFrame.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton.setBounds(22, 316, 105, 33);
		profilTab.add(btnNewButton);
		
		JButton btnGoToCourses = new JButton("GO TO Courses");
		btnGoToCourses.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new StudentAccountFrame1();
				setVisible(false);
			}
		});
		btnGoToCourses.setBounds(471, 340, 123, 23);
		profilTab.add(btnGoToCourses);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(StudentAccountFrame.class.getResource("/client/ui/student_icon.jpg")));
		label.setBounds(10, 11, 141, 213);
		profilTab.add(label);
		contentPane.setLayout(gl_contentPane);
	}
}
