package client.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Color;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

import client.delegate.courseManagement.CouseManagementDelegate;
import client.sessions.SessionTeacher;

import tn.edu.pdev.moocs.domain.Chapter;
import tn.edu.pdev.moocs.domain.Contain;
import tn.edu.pdev.moocs.domain.StaticText;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.awt.SystemColor;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;

public class ContainFrame extends JFrame {

	private JPanel contentPane;
	private JTextArea tb_cont; 
	JLabel lblAaaaaaa = new JLabel();
	StaticText sttext;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ContainFrame frame = new ContainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ContainFrame() {
		setVisible(true);
		setBounds(100, 100, 826, 483);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 240));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
		);
		List<StaticText>list = CouseManagementDelegate.findStaticTextByChapter(SessionTeacher.getInstance().getChapter());
		if(list.size()==0){
	  tb_cont = new JTextArea();
//		StaticText text = list.get(0);
//		tb_cont = new JTextArea(text.getTitleText());
	
	JButton btnNewButton = new JButton("Finish");
	btnNewButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			new ChaptersFrame();
			setVisible(false);
		}
	});
	
	JButton btnNewButton_1 = new JButton("save");
	btnNewButton_1.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			
			
			if(tb_cont.getText().equals(""))
			{
				JOptionPane.showMessageDialog(null, "Contain is empty", "Probl�me de format", JOptionPane.ERROR_MESSAGE);
				
			}else{
				sttext = new StaticText("text",tb_cont.getText());
				sttext.setChapter(SessionTeacher.getInstance().getChapter());
				sttext= (StaticText) CouseManagementDelegate.createContain(sttext);
			}
//
//			Chapter chapter = new Chapter(5);
//			Contain c1 = new Contain(6, "typeContain");
//			List<Contain> contains = new ArrayList<Contain>();
//			contains.add(c1);
//			
//			chapter.setContains(contains);
//			
//			c1.setChapter(chapter);
//			
//			CouseManagementDelegate.createChapter(chapter);
				
			}
			
//		}
	});
	
	JButton btnNewButton_2 = new JButton("clear");
	btnNewButton_2.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			
			tb_cont.setText("");
			
			
		}
	});
	
	JPanel panel_1 = new JPanel();
	panel_1.setBackground(UIManager.getColor("Button.disabledForeground"));
	GroupLayout gl_panel = new GroupLayout(panel);
	gl_panel.setHorizontalGroup(
		gl_panel.createParallelGroup(Alignment.LEADING)
			.addGroup(gl_panel.createSequentialGroup()
				.addGap(48)
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
						.addGap(367)
						.addComponent(btnNewButton_2, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
						.addGap(47)
						.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(panel_1, 0, 0, Short.MAX_VALUE)
						.addComponent(tb_cont, GroupLayout.DEFAULT_SIZE, 726, Short.MAX_VALUE)))
				.addContainerGap(26, Short.MAX_VALUE))
	);
	gl_panel.setVerticalGroup(
		gl_panel.createParallelGroup(Alignment.LEADING)
			.addGroup(gl_panel.createSequentialGroup()
				.addGap(6)
				.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addComponent(tb_cont, GroupLayout.PREFERRED_SIZE, 307, GroupLayout.PREFERRED_SIZE)
				.addGap(18)
				.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
					.addComponent(btnNewButton_1)
					.addComponent(btnNewButton_2)
					.addComponent(btnNewButton)))
	);
	
	JLabel label = new JLabel("");
	label.setIcon(new ImageIcon(ContainFrame.class.getResource("/client/ui/menu_arrow.gif")));
	
	lblAaaaaaa = new JLabel(SessionTeacher.getInstance().getThematic().getNameThematic());
	lblAaaaaaa.setForeground(UIManager.getColor("Button.disabledShadow"));
	lblAaaaaaa.setFont(new Font("Tahoma", Font.BOLD, 14));
	
	JLabel label_1 = new JLabel("");
	label_1.setIcon(new ImageIcon(ContainFrame.class.getResource("/client/ui/menu_arrow.gif")));
	
	JLabel label_2 = new JLabel("");
	label_2.setIcon(new ImageIcon(ContainFrame.class.getResource("/client/ui/menu_arrow.gif")));
	
	JLabel lblBbbb = new JLabel(SessionTeacher.getInstance().getCourse().getCourseName());
	lblBbbb.setFont(new Font("Tahoma", Font.BOLD, 14));
	lblBbbb.setForeground(UIManager.getColor("Button.disabledShadow"));
	
	JLabel label_3 = new JLabel("");
	label_3.setIcon(new ImageIcon(ContainFrame.class.getResource("/client/ui/menu_arrow.gif")));
	
	JLabel label_4 = new JLabel("");
	label_4.setIcon(new ImageIcon(ContainFrame.class.getResource("/client/ui/menu_arrow.gif")));
	
	JLabel label_5 = new JLabel("");
	label_5.setIcon(new ImageIcon(ContainFrame.class.getResource("/client/ui/menu_arrow.gif")));
	
	JLabel lblCccc = new JLabel(SessionTeacher.getInstance().getChapter().getTitle());
	lblCccc.setFont(new Font("Tahoma", Font.BOLD, 14));
	lblCccc.setForeground(UIManager.getColor("Button.disabledShadow"));
	GroupLayout gl_panel_1 = new GroupLayout(panel_1);
	gl_panel_1.setHorizontalGroup(
		gl_panel_1.createParallelGroup(Alignment.LEADING)
			.addGroup(gl_panel_1.createSequentialGroup()
				.addContainerGap()
				.addComponent(label)
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addComponent(lblAaaaaaa)
				.addGap(34)
				.addComponent(label_1)
				.addPreferredGap(ComponentPlacement.RELATED)
				.addComponent(label_2)
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addComponent(lblBbbb)
				.addGap(55)
				.addComponent(label_3)
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addComponent(label_4)
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addComponent(label_5)
				.addPreferredGap(ComponentPlacement.RELATED)
				.addComponent(lblCccc)
				.addContainerGap(309, Short.MAX_VALUE))
	);
	gl_panel_1.setVerticalGroup(
		gl_panel_1.createParallelGroup(Alignment.LEADING)
			.addGroup(gl_panel_1.createSequentialGroup()
				.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
					.addComponent(label)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAaaaaaa)
						.addComponent(label_1))
					.addComponent(label_2)
					.addComponent(label_3)
					.addComponent(label_4)
					.addComponent(label_5)
					.addComponent(lblBbbb)
					.addComponent(lblCccc))
				.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	);
	panel_1.setLayout(gl_panel_1);
	panel.setLayout(gl_panel);
	contentPane.setLayout(gl_contentPane);
		}else {
			StaticText text = list.get(0);
			tb_cont = new JTextArea(text.getTitleText());
		
		JButton btnNewButton = new JButton("Finish");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ChaptersFrame();
				setVisible(false);
			}
		});
		
		JButton btnNewButton_1 = new JButton("save");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				if(tb_cont.getText().equals(""))
				{
					JOptionPane.showMessageDialog(null, "Contain is empty", "Probl�me de format", JOptionPane.ERROR_MESSAGE);
					
				}else{
					sttext = new StaticText("text",tb_cont.getText());
					sttext.setChapter(SessionTeacher.getInstance().getChapter());
					sttext= (StaticText) CouseManagementDelegate.createContain(sttext);
				}
//	
//				Chapter chapter = new Chapter(5);
//				Contain c1 = new Contain(6, "typeContain");
//				List<Contain> contains = new ArrayList<Contain>();
//				contains.add(c1);
//				
//				chapter.setContains(contains);
//				
//				c1.setChapter(chapter);
//				
//				CouseManagementDelegate.createChapter(chapter);
					
				}
				
//			}
		});
		
		JButton btnNewButton_2 = new JButton("clear");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				tb_cont.setText("");
				
				
			}
		});
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(UIManager.getColor("Button.disabledForeground"));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(48)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
							.addGap(367)
							.addComponent(btnNewButton_2, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
							.addGap(47)
							.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
							.addComponent(panel_1, 0, 0, Short.MAX_VALUE)
							.addComponent(tb_cont, GroupLayout.DEFAULT_SIZE, 726, Short.MAX_VALUE)))
					.addContainerGap(26, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(6)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(tb_cont, GroupLayout.PREFERRED_SIZE, 307, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNewButton_1)
						.addComponent(btnNewButton_2)
						.addComponent(btnNewButton)))
		);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(ContainFrame.class.getResource("/client/ui/menu_arrow.gif")));
		
		lblAaaaaaa = new JLabel(SessionTeacher.getInstance().getThematic().getNameThematic());
		lblAaaaaaa.setForeground(UIManager.getColor("Button.disabledShadow"));
		lblAaaaaaa.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(ContainFrame.class.getResource("/client/ui/menu_arrow.gif")));
		
		JLabel label_2 = new JLabel("");
		label_2.setIcon(new ImageIcon(ContainFrame.class.getResource("/client/ui/menu_arrow.gif")));
		
		JLabel lblBbbb = new JLabel(SessionTeacher.getInstance().getCourse().getCourseName());
		lblBbbb.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblBbbb.setForeground(UIManager.getColor("Button.disabledShadow"));
		
		JLabel label_3 = new JLabel("");
		label_3.setIcon(new ImageIcon(ContainFrame.class.getResource("/client/ui/menu_arrow.gif")));
		
		JLabel label_4 = new JLabel("");
		label_4.setIcon(new ImageIcon(ContainFrame.class.getResource("/client/ui/menu_arrow.gif")));
		
		JLabel label_5 = new JLabel("");
		label_5.setIcon(new ImageIcon(ContainFrame.class.getResource("/client/ui/menu_arrow.gif")));
		
		JLabel lblCccc = new JLabel(SessionTeacher.getInstance().getChapter().getTitle());
		lblCccc.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCccc.setForeground(UIManager.getColor("Button.disabledShadow"));
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(label)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblAaaaaaa)
					.addGap(34)
					.addComponent(label_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(label_2)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblBbbb)
					.addGap(55)
					.addComponent(label_3)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(label_4)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(label_5)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCccc)
					.addContainerGap(309, Short.MAX_VALUE))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(label)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblAaaaaaa)
							.addComponent(label_1))
						.addComponent(label_2)
						.addComponent(label_3)
						.addComponent(label_4)
						.addComponent(label_5)
						.addComponent(lblBbbb)
						.addComponent(lblCccc))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
	}
}
}
