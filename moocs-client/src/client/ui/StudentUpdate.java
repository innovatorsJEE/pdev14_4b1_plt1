package client.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

import client.delegate.profilsManagement.ProfilsManagementDelegate;
import client.sessions.Session;

import com.toedter.calendar.JDateChooser;
import javax.swing.JComboBox;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.SystemColor;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.DefaultComboBoxModel;

import tn.edu.pdev.moocs.domain.Student;
import tn.edu.pdev.moocs.domain.Teacher;
import tn.edu.pdev.moocs.domain.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class StudentUpdate extends JFrame {

	private JPanel contentPane;
	private JTextField tf_fn;
	private JTextField tf_ln;
	private JTextField tf_cin;
	private JTextField tf_mail;
	private JTextField tf_log;
	private JPasswordField tf_pass;
	private JTextField tf_edu;
	private JLabel cb_sexe;
	private JComboBox comboBox;
	public static Student user= (Student) Session.getInstance().getUser();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StudentUpdate frame = new StudentUpdate();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StudentUpdate() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 536, 575);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setToolTipText("\r\n");
		panel.setBorder(new EmptyBorder(0, 0, 0, 0));
		panel.setBackground(Color.WHITE);
		
		cb_sexe = new JLabel("Sexe");
		cb_sexe.setForeground(Color.BLUE);
		cb_sexe.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		JLabel label_2 = new JLabel("CIN / Passport");
		label_2.setForeground(Color.BLUE);
		label_2.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		JLabel label_3 = new JLabel("Last Name");
		label_3.setForeground(Color.BLUE);
		label_3.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		JLabel label_4 = new JLabel("First Name \r\n");
		label_4.setForeground(Color.BLUE);
		label_4.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		JLabel label_5 = new JLabel("Date of birth");
		label_5.setForeground(Color.BLUE);
		label_5.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		JLabel label_6 = new JLabel("Mail\r\n");
		label_6.setForeground(Color.BLUE);
		label_6.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		JLabel label_7 = new JLabel("login\r\n");
		label_7.setForeground(Color.BLUE);
		label_7.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		JLabel label_8 = new JLabel("password");
		label_8.setForeground(Color.BLUE);
		label_8.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		JLabel label_9 = new JLabel("Education");
		label_9.setForeground(Color.BLUE);
		label_9.setFont(new Font("Stencil", Font.PLAIN, 16));
		
		tf_fn = new JTextField();
		tf_fn.setColumns(10);
		tf_fn.setBackground(new Color(255, 255, 240));
		tf_fn.setText(user.getFirstName());
		
		
		tf_ln = new JTextField();
		tf_ln.setColumns(10);
		tf_ln.setBackground(new Color(255, 255, 240));
		tf_ln.setText(user.getLastName());
		
		tf_cin = new JTextField();
		tf_cin.setEnabled(false);
		tf_cin.setColumns(10);
		tf_cin.setBackground(new Color(255, 255, 240));
		tf_cin.setText(Long.toString(user.getCin()));
		
		final JDateChooser dof = new JDateChooser();
		dof.setDate(new Date());
		final JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Man", "Women"}));
		comboBox.setToolTipText("Man");
		comboBox.setFont(new Font("Georgia", Font.BOLD, 11));
		
		tf_mail = new JTextField();
		tf_mail.setColumns(10);
		tf_mail.setBackground(new Color(255, 255, 240));
		tf_mail.setText(user.getMail());
		
		tf_log = new JTextField();
		tf_log.setColumns(10);
		tf_log.setBackground(new Color(255, 255, 240));
		tf_log.setText(user.getLogin());
		
		tf_pass = new JPasswordField();
		tf_pass.setBackground(new Color(255, 255, 240));
		tf_pass.setText(user.getPassword());
		
		tf_edu = new JTextField();
		tf_edu.setEnabled(false);
		tf_edu.setColumns(10);
		tf_edu.setBackground(new Color(255, 255, 240));
		tf_edu.setText(user.getEducation());
		
		JButton button = new JButton("Back");
		button.setForeground(Color.WHITE);
		button.setBackground(new Color(135, 206, 235));
		
		JButton btnNewButton = new JButton("Update");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			     Date ddn = dof.getDate();
			        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRANCE);
			        String dt = df.format(ddn);
				
				String fn = tf_fn.getText();
				String ln = tf_ln.getText();
				Long cin = Long.parseLong(tf_cin.getText());
				
			    String sexe = comboBox.getSelectedItem().toString();
			    String mail = tf_mail.getText();
			    String log = tf_log.getText();
			    String pass = tf_pass.getText();
			    String edu = tf_edu.getText();
			    
			    User user= Session.getInstance().getUser();
        		    
			    System.out.println(user.getId());
			    
			    user.setFirstName(fn);
			    user.setLastName(ln);
			    user.setCin(cin);
			    user.setSexe(sexe);
			    user.setMail(mail);
			    user.setLogin(log);
			    user.setPassword(pass);
			    user.setDateOfBirth(ddn);
			    
			    
			    ProfilsManagementDelegate.updateUser(user);
				
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 514, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(37, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		JButton btnNewButton_1 = new JButton("Back");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StudentAccountFrame accountFrame = new StudentAccountFrame();
				accountFrame.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton_1.setBackground(new Color(0, 255, 255));
		btnNewButton_1.setForeground(new Color(255, 255, 255));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(21)
					.addComponent(label_4)
					.addGap(61)
					.addComponent(tf_fn, GroupLayout.PREFERRED_SIZE, 288, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(21)
					.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
					.addGap(58)
					.addComponent(tf_ln, GroupLayout.PREFERRED_SIZE, 288, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(21)
					.addComponent(label_2)
					.addGap(39)
					.addComponent(tf_cin, GroupLayout.PREFERRED_SIZE, 288, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(21)
					.addComponent(label_5, GroupLayout.PREFERRED_SIZE, 153, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(dof, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(21)
					.addComponent(cb_sexe, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
					.addGap(39)
					.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(21)
					.addComponent(label_6, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
					.addGap(47)
					.addComponent(tf_mail, GroupLayout.PREFERRED_SIZE, 288, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(21)
					.addComponent(label_7, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
					.addGap(47)
					.addComponent(tf_log, GroupLayout.PREFERRED_SIZE, 288, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(21)
					.addComponent(label_8, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
					.addGap(63)
					.addComponent(tf_pass, GroupLayout.PREFERRED_SIZE, 288, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(21)
					.addComponent(label_9, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
					.addGap(47)
					.addComponent(tf_edu, GroupLayout.PREFERRED_SIZE, 288, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(192)
					.addComponent(button))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(213)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(btnNewButton)
						.addComponent(btnNewButton_1))
					.addGap(234))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(11)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(2)
							.addComponent(label_4))
						.addComponent(tf_fn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(20)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(2)
							.addComponent(label_3))
						.addComponent(tf_ln, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(20)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(2)
							.addComponent(label_2))
						.addComponent(tf_cin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(3)
							.addComponent(label_5))
						.addComponent(dof, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(2)
							.addComponent(cb_sexe))
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(27)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(2)
							.addComponent(label_6))
						.addComponent(tf_mail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(2)
							.addComponent(label_7))
						.addComponent(tf_log, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(26)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(1)
							.addComponent(label_8, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
						.addComponent(tf_pass, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(27)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(2)
							.addComponent(label_9))
						.addComponent(tf_edu, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(58)
					.addComponent(btnNewButton)
					.addGap(43)
					.addComponent(btnNewButton_1)
					.addGap(38)
					.addComponent(button))
		);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
	}
}
