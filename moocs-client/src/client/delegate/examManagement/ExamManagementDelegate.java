package client.delegate.examManagement;

import java.util.List;

import client.locator.ServiceLocator;

import tn.edu.pdev.moocs.domain.Answers;
import tn.edu.pdev.moocs.domain.Certification;
import tn.edu.pdev.moocs.domain.Chapter;
import tn.edu.pdev.moocs.domain.Question;
import tn.edu.pdev.moocs.domain.Quiz;
import tn.edu.pdev.moocs.domain.Thematic;
import tn.edu.pdev.moocs.services.examManagement.ExamManagementServiceRemote;

public class ExamManagementDelegate  {
	
	private static final String jndiName = "ejb:moocs/tn.edu.esprit.piDev.MOOCs/ExamManagementService!tn.edu.pdev.moocs.services.examManagement.ExamManagementServiceRemote" ;
	
	private static ExamManagementServiceRemote getproxy(){
		return (ExamManagementServiceRemote) ServiceLocator.getInstance().getProxy(jndiName);
	}
	
	public static void createCertification(Certification certification) {
		getproxy().createCertification(certification);
	}

	public static void createQuiz(Quiz quiz) {
		getproxy().createQuiz(quiz);
	}

	public static void createQuestion(Question question) {
		getproxy().createQuestion(question);
	}

	public static void createAnswers(Answers answers) {
		getproxy().createAnswers(answers);
	}

	public static void updateCertification(Certification certification) {
		getproxy().updateCertification(certification);
	}

	public static void updateQuiz(Quiz quiz) {
		getproxy().updateQuiz(quiz);
		
	}

	public static void updateQuestion(Question question) {
		getproxy().updateQuestion(question);
	}

	public static void updateAnswers(Answers answers) {
		getproxy().updateAnswers(answers);
	}

	public static Certification findCertification(int id) {
		return getproxy().findCertification(id);
	}

	public static Quiz findQuiz(int id) {
		return getproxy().findQuiz(id);
	}

	public static Question findQuestion(int id) {
		return getproxy().findQuestion(id);
	}

	public static Answers findAnswers(int id) {
		return getproxy().findAnswers(id);
	}
	public static Quiz findQuizByName (String nameQuiz){
		return getproxy().findQuizByName(nameQuiz);
	}

	public static void deleteCertification(Certification certification) {
		getproxy().deleteCertification(certification);
	}

	public static void deleteQuiz(Quiz quiz) {
		getproxy().deleteQuiz(quiz);
	}

	public static void deleteQuestion(Question question) {
		getproxy().deleteQuestion(question);
	}

	public static void deleteAnswers(Answers answers) {
		getproxy().deleteAnswers(answers);
	}

	public static List<Certification> findAllCertification() {
		return getproxy().findAllCertification();
	}

	public static List<Quiz> findAllQuiz() {
		return getproxy().findAllQuiz();
	}

	public static List<Question> findAllQuestion() {
		return getproxy().findAllQuestion();
	}

	public static List<Answers> findAllAnswers() {
		return getproxy().findAllAnswers();
	}
	public static List<Answers> getAnswersByQuestion(Question question){
		return getproxy().getAnswersByQuestion(question);
}
	public static List<Question> findAllQuestionByChap(Chapter chapter){
		return getproxy().findAllQuestionByChap(chapter);
	}

	public static List<Answers> finAllAnserwerByQuestion(Question question){
		return getproxy().finAllAnserwerByQuestion(question);
	}
	public static boolean certifExist(String thematic){
		return getproxy().certifExist(thematic);
	}
	public static List<Question> findAllQuestionByCertification(Certification certification){
		return getproxy().findAllQuestionByCertification(certification);
	}
	public static List<Question> findAllQuestionByThematic2(String name){
		return getproxy().findAllQuestionByThematic2(name);
	}
}
