package client.delegate.courseManagement;

import java.util.List;


import client.locator.ServiceLocator;

import tn.edu.pdev.moocs.domain.Chapter;
import tn.edu.pdev.moocs.domain.Contain;
import tn.edu.pdev.moocs.domain.Course;
import tn.edu.pdev.moocs.domain.Image;
import tn.edu.pdev.moocs.domain.Pdf;
import tn.edu.pdev.moocs.domain.StaticText;
import tn.edu.pdev.moocs.domain.Thematic;
import tn.edu.pdev.moocs.domain.Video;
import tn.edu.pdev.moocs.services.courseManagement.CouseManagementRemote;

public class CouseManagementDelegate {
	
	private static final String jndiName = "ejb:moocs/tn.edu.esprit.piDev.MOOCs/CouseManagement!tn.edu.pdev.moocs.services.courseManagement.CouseManagementRemote" ;
	
	private static CouseManagementRemote getproxy(){
		return (CouseManagementRemote) ServiceLocator.getInstance().getProxy(jndiName);
	}

	public static void createCourse(Course course) {
		getproxy().createCourse(course);
		
	}

	public static void createThematic(Thematic thematic) {
		getproxy().createThematic(thematic);
	}

	public static void createChapter(Chapter chapter) {
		getproxy().createChapter(chapter);
	}

	public static void createContain(Contain contain) {
		getproxy().createContain(contain);
	}

	public static void updateCourse(Course course) {
		getproxy().updateCourse(course);
	}

	public static void updateThematic(Thematic thematic) {
		getproxy().updateThematic(thematic);
	}

	public static void updateChapter(Chapter chapter) {
		getproxy().updateChapter(chapter);
	}

	public static void updateContain(Contain contain) {
		getproxy().updateContain(contain);
	}

	public static Course findCourse(int id) {
		return getproxy().findCourse(id);
	}

	public static Thematic findThematic(int id) {
		return getproxy().findThematic(id);
	}

	public static Chapter findChapter(int id) {
		return getproxy().findChapter(id);
	}

	public static StaticText findStaticText(int id) {
		return getproxy().findStaticText(id);
	}

	public static Pdf findPdf(int id) {
		return getproxy().findPdf(id);
	}

	public static Video findVideo(int id) {
		return getproxy().findVideo(id);
	}

	public static Image findImage(int id) {
		return getproxy().findImage(id);
	}

	public static void deleteCourse(Course course) {
		getproxy().deleteCourse(course);
	}

	public static void deleteThematic(Thematic thematic) {
		getproxy().deleteThematic(thematic);
	}

	public static void deleteChapter(Chapter chapter) {
		getproxy().deleteChapter(chapter);
	}

	public static void deleteContain(Contain contain) {
		getproxy().deleteContain(contain);
	}

	public static List<Course> findAllCourse() {
		return getproxy().findAllCourse();
	}

	public static List<Thematic> findAllThematic() {
		return getproxy().findAllThematic();
	}

	public static List<Chapter> findAllChapter() {
		return getproxy().findAllChapter();
	}

	public static List<StaticText> findAllStaticText() {
		return getproxy().findAllStaticText();
	}

	public static List<Pdf> findAllPdf() {
		return getproxy().findAllPdf();
	}

	public static List<Video> findAllVideo() {
		return getproxy().findAllVideo();
	}

	public static List<Image> findAllImage() {
		return getproxy().findAllImage();
	}
	public static List<Chapter> findAllChapter(Course course) {

       return getproxy().findAllChapter(course);
	}
	
	public static List<StaticText> findAllContain(Course course){
		return getproxy().findAllContain(course);
	}

}
