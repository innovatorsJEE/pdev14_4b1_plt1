package client.delegate.profilsManagement;

import java.util.List;

import client.locator.ServiceLocator;

import tn.edu.pdev.moocs.domain.Admin;
import tn.edu.pdev.moocs.domain.Student;
import tn.edu.pdev.moocs.domain.Teacher;
import tn.edu.pdev.moocs.domain.User;
import tn.edu.pdev.moocs.services.profilsManagement.ProfilsManagementServiceRemote;

public class ProfilsManagementDelegate {

	private static final String jndiName = "ejb:moocs/tn.edu.esprit.piDev.MOOCs/ProfilsManagementService!tn.edu.pdev.moocs.services.profilsManagement.ProfilsManagementServiceRemote" ;
	
	private static ProfilsManagementServiceRemote getproxy(){
		
		return  (ProfilsManagementServiceRemote) ServiceLocator.getInstance().getProxy(jndiName);
	}

	public static User createUser(User user) {
		return getproxy().createUser(user);
	}

	public static void updateUser(User user) {
		getproxy().updateUser(user);
	}

	public static Student findStudent(int id) {
		return getproxy().findStudent(id);
	}

	public static Teacher findTeacher(int id) {
		return getproxy().findTeacher(id);
	}

	public static Admin findAdmin(int id) {
		return getproxy().findAdmin(id);
	}

	public static void deleteUser(User user) {
		getproxy().deleteUser(user);
	}

	public static List<Student> findAllStudent() {
		return getproxy().findAllStudent();
	}

	public static List<Teacher> findAllTeacher() {
		return getproxy().findAllTeacher();
	}

	public static List<Admin> findAllAdmin() {
		return getproxy().findAllAdmin();
	}
	public static List<User> findAllUser() {
		
		return getproxy().findAllUser();
	}


	public static User autehnticate(String login, String password) {
		
		return getproxy().autehnticate(login, password);
	}





}
