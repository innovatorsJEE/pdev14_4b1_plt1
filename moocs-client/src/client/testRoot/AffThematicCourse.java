package client.testRoot;

import tn.edu.pdev.moocs.domain.Course;
import tn.edu.pdev.moocs.domain.Thematic;
import client.delegate.courseManagement.CouseManagementDelegate;

public class AffThematicCourse {

	public static void main(String[] args) {
		
		
		Thematic thematic = CouseManagementDelegate.findThematic(12);

	
		Course course1 = CouseManagementDelegate.findCourse(3);	

		course1.setThematic(thematic);
		
		CouseManagementDelegate.updateCourse(course1);
		
	}
	
	
}
