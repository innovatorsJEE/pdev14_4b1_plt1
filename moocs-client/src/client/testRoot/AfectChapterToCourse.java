package client.testRoot;

import java.util.List;

import client.delegate.courseManagement.CouseManagementDelegate;
import tn.edu.pdev.moocs.domain.Chapter;
import tn.edu.pdev.moocs.domain.Course;

public class AfectChapterToCourse {

	public static void main(String[] args) {
		
	//	Chapter chapter = CouseManagementDelegate.findChapter(4);
		Chapter chapter2 = CouseManagementDelegate.findChapter(17);
		Chapter chapter3 = CouseManagementDelegate.findChapter(18);
		Chapter chapter4 = CouseManagementDelegate.findChapter(19);
		
		Course course = CouseManagementDelegate.findCourse(14);
		
//      chapter.setCourse(course);
      chapter2.setCourse(course);
      chapter3.setCourse(course);
      chapter4.setCourse(course);
      
  //    CouseManagementDelegate.updateChapter(chapter);
      CouseManagementDelegate.updateChapter(chapter2);
      CouseManagementDelegate.updateChapter(chapter3);
      CouseManagementDelegate.updateChapter(chapter4);
		
	}
}
