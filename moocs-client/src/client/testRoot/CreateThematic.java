package client.testRoot;

import client.delegate.courseManagement.CouseManagementDelegate;
import tn.edu.pdev.moocs.domain.Thematic;

public class CreateThematic {

	public static void main(String[] args) {
		
		Thematic thematic = new Thematic("Informatique");
		
		CouseManagementDelegate.createThematic(thematic);
	}
}
