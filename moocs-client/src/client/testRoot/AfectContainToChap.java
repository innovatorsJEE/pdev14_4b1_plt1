package client.testRoot;

import client.delegate.courseManagement.CouseManagementDelegate;
import tn.edu.pdev.moocs.domain.Chapter;
import tn.edu.pdev.moocs.domain.StaticText;

public class AfectContainToChap {

	public static void main(String[] args) {
		
		Chapter chapter = CouseManagementDelegate.findChapter(15);
		Chapter chapter2 = CouseManagementDelegate.findChapter(17);
		Chapter chapter3 = CouseManagementDelegate.findChapter(18);
		Chapter chapter4 = CouseManagementDelegate.findChapter(19);
		
		StaticText text1 = CouseManagementDelegate.findStaticText(20);
		StaticText text2 = CouseManagementDelegate.findStaticText(21);
		StaticText text3 = CouseManagementDelegate.findStaticText(22);
		StaticText text4 = CouseManagementDelegate.findStaticText(23);
		
		text1.setChapter(null);
		text2.setChapter(null);
		text3.setChapter(chapter3);
		text4.setChapter(chapter4);
		
		CouseManagementDelegate.updateContain(text1);
		CouseManagementDelegate.updateContain(text2);
		CouseManagementDelegate.updateContain(text3);
		CouseManagementDelegate.updateContain(text4);
		
	}
}
