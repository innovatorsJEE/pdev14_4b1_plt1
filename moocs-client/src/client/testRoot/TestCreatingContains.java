package client.testRoot;

import client.delegate.courseManagement.CouseManagementDelegate;
import tn.edu.pdev.moocs.domain.Contain;
import tn.edu.pdev.moocs.domain.StaticText;

public class TestCreatingContains {

	public static void main(String[] args) {

		
		Contain contain = new StaticText( "Introduction", "The first step in learning about the application server will be installing all thenecessary stuff on your machine in order to run it.");
		Contain contain1 = new StaticText( "2", " The structure of the application server is maintained into a single file, which acts as a main reference for all server configurations.");
		Contain contain2 = new StaticText( "3", " This chapter completes the configuration of the application server, by adding a comprehensive description of the Java Enterprise services");
		Contain contain3 = new StaticText( "Conclusion", " in the conclusion");

	   CouseManagementDelegate.createContain(contain);
	   CouseManagementDelegate.createContain(contain1);
	   CouseManagementDelegate.createContain(contain2);
	   CouseManagementDelegate.createContain(contain3);
	}
	
	
}
