package client.testRoot;

import java.util.List;

import client.delegate.courseManagement.CouseManagementDelegate;
import tn.edu.pdev.moocs.domain.Chapter;
import tn.edu.pdev.moocs.domain.Contain;
import tn.edu.pdev.moocs.domain.Course;
import tn.edu.pdev.moocs.domain.StaticText;

public class ShowCourse {

	public static void main(String[] args) {
		
			Course course = CouseManagementDelegate.findCourse(26);
			

			Chapter chapter1 = CouseManagementDelegate.findChapter(29);
			
			chapter1.setHasQuiz(false);
			
			CouseManagementDelegate.updateChapter(chapter1);
			
			
			
			List<Chapter> chapters = CouseManagementDelegate.findAllChapter(course);
			

			for (Chapter chapter : chapters) {
				System.out.println(chapter);
			}
			
	}
}
