package client.sessions;

import tn.edu.pdev.moocs.domain.Chapter;
import tn.edu.pdev.moocs.domain.Course;
import tn.edu.pdev.moocs.domain.Question;
import tn.edu.pdev.moocs.domain.Thematic;
import tn.edu.pdev.moocs.domain.User;

public class SessionTeacher {

	private static SessionTeacher instance;
	
	private Thematic thematic;
	private Course course;
	private Chapter chapter;
	private Question question;
	


	public SessionTeacher() {
	
	}

	public static SessionTeacher getInstance() {
		
		if(instance == null)
		{
			instance= new SessionTeacher();
		}
		return instance;
	}
	
	
	public Thematic getThematic() {
		return thematic;
	}
	
	public void setThematic(Thematic thematic) {
		this.thematic = thematic;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Chapter getChapter() {
		return chapter;
	}

	public void setChapter(Chapter chapter) {
		this.chapter = chapter;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}


}
