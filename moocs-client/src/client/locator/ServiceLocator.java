package client.locator;

import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ServiceLocator {
	
	public static ServiceLocator instance ;
	
	private Context context;
	private Map<String, Object> cache ;
	
	
	private ServiceLocator() {
		cache = new HashMap<String,Object>();
		
		try {
			context = new InitialContext();
		} catch (NamingException e) {
			throw new ServiceLocatorException(e);
		}
	}
	public synchronized Object getProxy(String jndi){
		Object proxy;
		proxy = cache.get(jndi);
		
		if(proxy==null){
			try {
				proxy = context.lookup(jndi);
			} catch (NamingException e) {
				throw new ServiceLocatorException(e);
			}
			cache.put(jndi, proxy);
		}
		
		return proxy;
	}
	public static ServiceLocator getInstance() {
		
		if(instance == null){
			instance = new ServiceLocator();
		}
		
		return instance;
	}

}
