package config;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import tn.edu.pdev.moocs.domain.Admin;
import tn.edu.pdev.moocs.domain.Student;
import tn.edu.pdev.moocs.domain.Teacher;
import tn.edu.pdev.moocs.services.profilsManagement.ProfilsManagementServiceLocal;



@Singleton
@Startup
public class ConfigBean {
	
	@EJB
	private ProfilsManagementServiceLocal authentication;

    public ConfigBean() {
    }
    
    @PostConstruct
    public void createData(){
    	if (!authentication.loginExists("saadi")) {
    		authentication.createUser(new Teacher("saadi", null, null, null, null, null, "saadi", "saadi", null));
		}
    }
    
    @PostConstruct
    public void createData2(){
    	if (!authentication.loginExists("Zoubeir")) {
    		authentication.createUser(new Teacher("Zoubeir", "Zoubeir", null, null, null, null, "Zoubeir", "Zoubeir", null));
		}
    }
}
