package tn.edu.pdev.moocs.services.examManagement;


import java.util.List;

import javax.ejb.Remote;

import tn.edu.pdev.moocs.domain.Admin;
import tn.edu.pdev.moocs.domain.Answers;
import tn.edu.pdev.moocs.domain.Certification;
import tn.edu.pdev.moocs.domain.Chapter;
import tn.edu.pdev.moocs.domain.Contain;
import tn.edu.pdev.moocs.domain.Course;
import tn.edu.pdev.moocs.domain.Image;
import tn.edu.pdev.moocs.domain.Pdf;
import tn.edu.pdev.moocs.domain.Question;
import tn.edu.pdev.moocs.domain.Quiz;
import tn.edu.pdev.moocs.domain.StaticText;
import tn.edu.pdev.moocs.domain.Student;
import tn.edu.pdev.moocs.domain.Teacher;
import tn.edu.pdev.moocs.domain.Thematic;
import tn.edu.pdev.moocs.domain.User;
import tn.edu.pdev.moocs.domain.Video;

@Remote
public interface ExamManagementServiceRemote {
	public void createCertification(Certification certification);
	public void createQuiz(Quiz quiz);
	public void createQuestion(Question question);
	public void createAnswers(Answers answers);
	
	public void updateCertification(Certification certification);
	public void updateQuiz(Quiz quiz);
	public void updateQuestion(Question question);
	public void updateAnswers(Answers answers);
	
	public Certification findCertification( int id);
	public Quiz findQuiz(int id );
	public Question findQuestion(int id);
	public Answers findAnswers(int id);
	public Quiz findQuizByName(String nameQuiz);
	
	
	public void deleteCertification(Certification certification);
	public void deleteQuiz(Quiz quiz);
	public void deleteQuestion(Question question);
	public void deleteAnswers(Answers answers);
	
	
	public List<Certification> findAllCertification();
	public List<Quiz> findAllQuiz();
	public List<Question> findAllQuestion();
	public List<Answers> findAllAnswers();
	public List<Answers> getAnswersByQuestion(Question question);
	
	public List<Question> findAllQuestionByChap(Chapter chapter);
	public List<Answers> finAllAnserwerByQuestion(Question question);
	public boolean certifExist(String thematic);
	public List<Question> findAllQuestionByCertification(Certification certification);
	public List<Question> findAllQuestionByThematic(String thematic);
	public List<Question> findAllQuestionByThematic2(String name);
}
