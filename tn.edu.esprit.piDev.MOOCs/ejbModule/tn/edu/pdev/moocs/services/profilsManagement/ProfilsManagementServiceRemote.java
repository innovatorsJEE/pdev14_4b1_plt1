package tn.edu.pdev.moocs.services.profilsManagement;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.pdev.moocs.domain.Admin;
import tn.edu.pdev.moocs.domain.Course;
import tn.edu.pdev.moocs.domain.Quiz;
import tn.edu.pdev.moocs.domain.Student;
import tn.edu.pdev.moocs.domain.Teacher;
import tn.edu.pdev.moocs.domain.User;

@Remote
public interface ProfilsManagementServiceRemote {

	public User createUser(User user);
	
	public void updateUser(User user);
	
	public Student findStudent(int id);
	public Teacher findTeacher(int id);
	public Admin findAdmin(int id);
	
	public void deleteUser(User user);
	
	public List<Student> findAllStudent();
	public List<Teacher> findAllTeacher();
	public List<Admin> findAllAdmin();
	public List<User> findAllUser();
	
	User autehnticate(String login,String password);
	

}
